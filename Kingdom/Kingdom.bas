   5 REM "P"
  10 PRINT "   \ .\. \ .\. \..\ .\.  \..\ .\..\..\. \..\..\. \ .\..\..\. \ .\.  \ .\. "
  15 PRINT "   \ :\: \ :\: \::\ :\:. \::\ :\:'\':\: \::\''\':\ :\: \ :\: \ :\:. \.:\: "
  20 PRINT "   \ :\:.\::\' \::\ :\::\:.\::\ :\:   \:: \ :\ :\: \ :\: \ :\:'\::\':\: "
  25 PRINT "   \ :\:'\::\. \::\ :\: \':\::\ :\: \ .\. \:: \ :\ :\: \ :\: \ :\:  \ :\: "
  30 PRINT "   \ :\: \ :\: \::\ :\:  \::\ :\:.\..\: \::\..\.:\ :\:.\.:\: \ :\:  \ :\: "
  35 PRINT "   \ '\' \ '\' \''\ '\'  \''\ '\''\''\' \''\''\' \ '\''\''\' \ '\'  \ '\' "
  40 PRINT ,,
  50 PRINT "COPYRIGHT 1982-M0RGAN ASSOCIATES"
  60 PRINT AT 16,0;"DO YOU WANT INSTRUCTIONS ? Y/N"
  70 IF INKEY$ ="" THEN GOTO 70
  80 IF INKEY$ ="Y" THEN GOSUB 8990
 230 LET Y=0
 240 LET C=5000
 242 LET Q=INT (RND*3)+1
 244 IF Q=1 THEN LET C=C-250
 246 IF Q=2 THEN LET C=C-400
 248 IF Q=3 THEN LET C=C+150
 250 LET S=1000
 252 LET W=INT (RND*3)+1
 254 IF W=1 THEN LET S=S+50
 256 IF W=2 THEN LET S=S+80
 258 IF W=3 THEN LET S=S-75
 260 LET L=200
 300 LET Y=Y+1
 340 CLS
 345 LET K=0
 350 PRINT ,,"           %Y%E%A%R  ";Y
 360 PRINT 
 370 PRINT "  YOUR STORE NOW CONTAINS ";C
 380 PRINT "         SACKS OF CORN"
 390 PRINT 
 400 PRINT "     YOU HAVE ";S;" SUBJECTS"
 410 PRINT 
 420 PRINT "   YOU HAVE ";L;" ACRES OF LAND"
 425 PRINT 
 430 PRINT "\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
 440 PRINT 
 445 IF Y=21 THEN GOTO 1810
 450 PRINT "   HOW MANY SACKS OF CORN"
 460 PRINT "     DO YOU WANT PLANTED?"
 470 PRINT 
 480 INPUT P
 490 IF NOT P>C THEN GOTO 520
 500 PRINT "    YOU HAVE NOT GOT THAT MUCH"
 505 LET K=K+1
 508 IF K=4 THEN GOTO 340
 510 GOTO 480
 520 IF NOT P>2*S THEN GOTO 550
 530 PRINT "NOT ENOUGH PEOPLE TO PLANT IT"
 535 LET K=K+1
 538 IF K=4 THEN GOTO 340
 540 GOTO 480
 550 IF NOT P>8*L THEN GOTO 580
 560 PRINT "NOT ENOUGH LAND"
 565 LET K=K+1
 568 IF K=4 THEN GOTO 340
 570 GOTO 480
 580 PRINT 
 585 LET C=C-P
 600 PRINT "   HOW MANY SACKS OF CORN DO"
 610 PRINT "   YOU WANT GIVEN OUT AS FOOD"
 620 PRINT "   YOU HAVE ";C
 622 PRINT 
 630 INPUT F
 640 IF NOT F>C THEN GOTO 670
 650 PRINT "    YOU HAVE NOT GOT THAT MUCH"
 655 LET K=K+1
 657 IF K=5 THEN LET C=C+P
 658 IF K=5 THEN GOTO 340
 660 GOTO 630
 670 LET C=C-F
 680 CLS
 700 LET Z=1
 710 LET A=P/8
 720 LET X=L*3/4
 730 IF A<X THEN LET Z=-1
 740 LET G=INT (Z*L/4)
 750 LET L=L+G
 760 LET ZP=1
 770 LET M=1
 780 LET E=F/4
 790 IF E<S THEN LET ZP=-1
 800 LET GP=(E-S)*ZP
 810 LET X=S*3/4
 820 IF E<X THEN LET M=-1
 830 LET S=E
 840 RAND 
 850 LET X=INT (RND*3)+1
 900 IF Z<0 THEN GOTO 940
 910 PRINT "  YOU HAVE INCREASED YOUR LAND"
 920 PRINT ,"BY ";G;" ACRES"
 930 GOTO 950
 940 PRINT "    YOU HAVE LOST ";G*Z;" ACRES"
 950 PRINT 
 951 IF L>12 THEN GOTO 960
 952 PRINT "\:'\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\':"
 953 PRINT "\:                               \ :"
 954 PRINT "\:      YOU HAVE LOST THE GAME   \ :"
 955 PRINT "\:       DUE TO LACK OF LAND     \ :"
 956 PRINT "\:                               \ :"
 957 PRINT "\:.\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\.:"
 958 STOP
 960 IF GP=0 THEN GOTO 1290
 970 IF ZP<0 THEN GOTO 1000
 980 PRINT "YOU HAVE GAINED ";GP;" SUBJECTS"
 990 GOTO 1010
1000 PRINT "    ";GP;" PEOPLE HAVE STARVED"
1010 PRINT 
1020 IF S>24 THEN GOTO 1100
1030 PRINT "\:'\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\':"
1040 PRINT "\:                               \ :"
1050 PRINT "\:     YOU HAVE LOST THE GAME    \ :"
1060 PRINT "\:   DUE TO A LACK OF SUBJECTS   \ :"
1070 PRINT "\:                               \ :"
1080 PRINT "\:.\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\.:"
1090 GOTO 2000
1100 IF M>0 THEN GOTO 1290
1110 PRINT "\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
1115 PRINT 
1120 PRINT "  TOO MANY PEOPLE ARE STARVING "
1130 PRINT "THERE IS AN ASSASINATION ATTEMPT"
1135 PRINT 
1140 PRINT "\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
1150 PRINT 
1160 PRINT "PRESS ANY KEY TO SEE IF YOU "
1170 PRINT "SURVIVE..."
1180 IF INKEY$ ="" THEN GOTO 1175
1200 IF X>1 THEN GOTO 1270
1210 PRINT ,,,"  \:'\''\''\''\''\''\''\''\''\''\':"
1220 PRINT ,"  \: ";"         \ :"
1230 PRINT ,"  \: ";" YOU DIED";"\ :"
1240 PRINT ,"  \: ";"         \ :"
1250 PRINT ,"  \:.\..\..\..\..\..\..\..\..\..\.:"
1252 FOR A=1 TO 30
1254 NEXT A
1260 GOTO 2500
1270 PRINT ,,,,"     **********************"
1280 PRINT "     * OK START BREATHING *"
1285 PRINT "     **********************"
1290 PRINT 
1300 PRINT "   PRESS ANY KEY TO CONTINUE"
1310 IF INKEY$ ="" THEN GOTO 1310
1320 CLS
1400 IF P<1 THEN GOTO 300
1410 LET W=1
1420 LET B=1
1425 IF P<20 THEN GOTO 1580
1430 LET Q=INT (RND*6)+1
1440 LET H=(P/10)*2
1450 IF Q>2 THEN LET H=(P/10)*4
1460 IF Q=6 THEN LET H=(P/10)*8
1470 IF NOT H>(S/10)*16 THEN GOTO 1530
1480 LET B=-1
1490 LET N=H-(S/10)*16
1500 IF N>3200 THEN LET N=3200
1510 LET N=N*10
1520 LET H=(S/10)*16
1530 IF C/10+H>3200 THEN GOTO 1560
1540 LET C=C+H*10
1550 GOTO 1600
1560 LET W=-1
1570 GOTO 1600
1580 LET Q=3
1590 LET C=P*2+C
1620 PRINT 
1630 IF Q<3 THEN PRINT ,,,,,,"  THE HARVEST HAS BEEN POOR"
1640 IF Q>=3 AND Q<6 THEN PRINT ,,,,,,"  THE HARVEST HAS BEEN AVERAGE"
1650 IF Q=6 THEN PRINT ,,,,,,"   THE HARVEST HAS BEEN GOOD"
1660 PRINT 
1670 PRINT "\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
1675 PRINT 
1680 IF B>0 THEN GOTO 1710
1690 PRINT "  ";N;" LOST SACKS DUE TO LACK"
1700 PRINT ,"  OF MANPOWER"
1705 PRINT 
1710 IF W>0 THEN GOTO 1770
1720 FOR F=1 TO 21
1724 PRINT AT 9,0;"                                "
1728 PRINT AT 9,0;"********************************"
1730 PRINT "*  YOU HAVE OVER 32000 SACKS   *"
1740 PRINT "*      OF CORN SO YOU WIN      *" 
1745 PRINT AT 12,0;"                                "
1750 PRINT AT 12,0;"********************************"
1755 NEXT F
1760 GOTO 2000
1770 PRINT ,,,,"   PRESS ANY KEY TO CONTINUE"
1780 IF INKEY$ ="" THEN GOTO 1780
1790 CLS
1800 GOTO 300
1810 FOR K=1 TO 21
1816 PRINT AT 11,0;"                                "
1818 PRINT AT 11,0;"********************************"
1820 PRINT "*  YOU HAVE SURVIVED 20 YEARS  *"
1830 PRINT "*          SO YOU WIN          *"
1835 PRINT AT 14,0;"                                "
1840 PRINT AT 14,0;"********************************"
1850 NEXT K
2000 PRINT AT 19,0;"     PRESS \"R\" TO PLAY AGAIN"
2010 IF INKEY$ ="" THEN GOTO 2010
2020 IF INKEY$ ="R" THEN GOTO 230
2030 STOP
2500 CLS
2510 PRINT AT 5,4;"          \.:\:."
2520 PRINT "              \ :\: "
2530 PRINT "           \:'\''\''\''\''\''\':\.              "
2540 PRINT "           \:      \ :\'.            "
2550 LET L$="           \:      \ :\ :             "
2560 PRINT L$;L$;L$;L$;L$;L$;L$
2565 FOR L=1 TO 16
2570 PRINT "////////";
2575 NEXT L
2578 FOR M=1 TO 41
2580 PRINT AT 10,13;"   "
2584 PRINT AT 10,13;"RIP"
2586 NEXT M
2590 PRINT AT 20,7;"GOOD-BYE SUCKER"
2600 STOP
8990 CLS
8995 GOSUB 9150
9000 PRINT ,,"YOU ARE THE RULER OF A SMALL    MEDIEVAL KINGDOM. YOU HAVE TO   DECIDE HOW MUCH CORN TO SOW AND"
9010 PRINT "HOW MUCH TO USE TO FEED YOUR    SUBJECTS."
9020 PRINT AT 20,5;"ANY KEY TO CONTINUE"
9030 IF INKEY$ ="" THEN GOTO 9030
9040 CLS
9050 PRINT "%N%.%B%. IT TAKES FOUR SACKS OF CORN"
9060 PRINT "TO FEED ONE PERSON FOR ONE"
9070 PRINT "YEAR. IT TAKES EIGHT SACKS TO"
9080 PRINT "PLANT ONE ACRE. ONE PERSON"
9090 PRINT "CAN PLANT 2 SACKS."
9100 PRINT ,,,,"YOU HAVE TO SURVIVE 20 YEARS OR ACCUMULATE 32,000 SUPLUS SACKS  OF CORN TO WIN"
9106 PRINT ,,,,"%W%A%R%N%I%N%G%: IF YOU DONT SOW ENOUGH CORN YOU CANNOT EXPECT TO REAP AGOOD CROP"
9108 PRINT "BUT IF YOU DONT FEED THE PEOPLE THEY TURN ON YOU -YOU HAVE BEEN WARNED"
9110 PRINT ,,,,,,"       OK PRESS ANY KEY"
9120 IF INKEY$ ="" THEN GOTO 9120
9130 CLS
9132 PRINT AT 5,0;"%N%.%B%. REMEMBER:"
9134 PRINT ,,"EACH PEASANT CONSUMES    4 SACKSCAN ONLY PLANT           2 SACKSNUMBER TO SOW PER ACRE   8 SACKS"
9136 PRINT ,,,,,,,,"PRESS ANY KEY TO BEGIN THE GAME"
9138 IF INKEY$ ="" THEN GOTO 9138
9139 CLS
9140 RETURN
9150 DIM A$(6,32)
9160 LET A$(1)="\:: \:: \::                     \:: \:: \::"
9170 LET A$(2)="\ '\::\::\::\'                      \ '\::\::\::\' "
9180 LET A$(3)=" \:: \::                       \:: \:: "
9190 LET A$(4)=" \::\::\:: \:: \:: \:: \:: \:: \:: \:: \:: \:: \:: \:: \::\::\:: "
9200 LET A$(5)=" \::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::"
9210 LET A$(6)=" \::\::\::\::\::\::\::\::\::\::\::\::    \::\::\::\::\::\::\::\::\::\::\::\::\::"
9260 PRINT A$(1);A$(2);A$(3);A$(3);A$(4);A$(5);A$(5);A$(5);A$(6);A$(6);A$(6);A$(6)
9270 RETURN
