#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
void in_Pause(uint msec);
char strlen(const char *s);
uint in_KeyPressed(uint scancode);
int in_Wait(uint msec);
unsigned int in_Inkey(void);
void in_WaitForKey(void);
void in_WaitForNoKey(void);

int in_InKey(const char *s);

uchar GetChar(char cX, char cY)
{
	return wpeek(wpeek(16396) + 1 + cY + cX * 33);
}

void SetChar(char cX, char cY, char cValue) {
	bpoke(wpeek(16396) + 1 + cY + cX * 33, cValue);
}

void deleteBloq (char x, char y)
{
	SetChar(x, y, 0);
}

void printBloq (char x, char y)
{
	SetChar(x, y, 128);
}

void printHead (char x, char y)
{
	SetChar(x, y, 139);
}

void printApple (char x, char y)
{
	SetChar(x, y, 141);
}

struct controls {
	unsigned char up;
	unsigned char down;
	unsigned char left;
	unsigned char right;
} k1;

unsigned char start;

unsigned char menu_define_key() {
	//https://bitbucket.org/CmGonzalez/gandalf/src/0af70239a352257743d21292715470d116b2c4ab/game_menu.c#lines-199
	unsigned char tmp1;
    in_WaitForKey();
    tmp1 = in_Inkey();
    in_WaitForNoKey();
    return tmp1;
}

void printCenter(char numLiner, char* textCenter) {
	char lenCenter = strlen(textCenter);
	char whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner, whiteSpace);
	printf("%s", textCenter);
}

int main(void)
{
	k1.up = 'Q';
	k1.down = 'A';
	k1.left = 'O';
	k1.right = 'P';

	unsigned char snakeX = 3, snakeY = 13, tailX = 3, tailY = 10, check = 0, appleX = 5, appleY = 20, sleep = 50, mx = 0, r1 = 0, r2 = 0;
	char vx = 0, vy = 1, nx = 0, ny = 0, onx = 0, ony = 0;
	unsigned int size = 3, hiscore = 0;

RESTART:
	
	MENU:
	zx_cls();
	zx_asciimode(1);
	printCenter(3, "SNAKE");
	printCenter(6, "participated in the");
	printCenter(7, "retro programmers inside");
	printCenter(8, "(rpi) and phaze101 game jam");
	zx_asciimode(0);
	zx_setcursorpos(2, 12);
	printf("%c%c%c%c%c%c%c", 128,128,128,128,128,128,128);
	zx_setcursorpos(3, 12);
	printf("%c", 128);
	zx_setcursorpos(3, 18);
	printf("%c", 128);
	zx_setcursorpos(4, 12);
	printf("%c%c%c%c%c%c%c", 128,128,128,128,128,128,128);
	zx_asciimode(1);
	printCenter(12, "press \"R\" to redefine keys");
	printCenter(14, "press \"I\" to instructions");
	printCenter(16, "press \"S\" to start");

	if (hiscore > 0) {
		printCenter(19, "hi-score:     ");
		zx_setcursorpos(19, 19);
		printf("%d", hiscore);
	}

	printCenter(23, "2023 salvacam");

SELECTOPTION:
	start = menu_define_key();

	if (start == 's') {
		goto RESET;
	}

	if (start == 'I') {

		zx_cls();
		zx_setcursorpos(2, 5);
		printf("control the snake");
		zx_setcursorpos(5, 5);
		printf("avoid the tail");		
		zx_setcursorpos(8, 5);
		printf("and walls");
		zx_setcursorpos(11, 5);
		printf("eat the money");

		zx_asciimode(0);
		zx_setcursorpos(2, 23);
		printf("%c%c%c%c", 128, 128, 128, 139);
		zx_setcursorpos(5,23);
		printf("%c%c%c", 128, 128, 128);
		zx_setcursorpos(8,23);
		printf("%c", 136);
		zx_setcursorpos(11,23);
		printf("%c", 141);

		zx_asciimode(1);

		printCenter(17, "controls");
		zx_setcursorpos(19, 5);
		printf("up:   %c", k1.up);
			
		zx_setcursorpos(21, 5);
		printf("down: %c", k1.down);

		zx_setcursorpos(19, 16);
		printf("left:  %c", k1.left);
		
		zx_setcursorpos(21, 16);
		printf("right: %c", k1.right);

		fgetc_cons();        // wait for keypress
		goto MENU;

	} 
	else if (start == 'R') {
		REMAP:
		zx_cls();

		zx_setcursorpos(5, 5);
		printf("up:   ");
		k1.up = menu_define_key();
		printf("%c", k1.up);
			
		zx_setcursorpos(7, 5);
		printf("down: ");
		k1.down = menu_define_key();
		printf("%c", k1.down);

		zx_setcursorpos(5, 15);
		printf("left:  ");
		k1.left = menu_define_key();
		printf("%c", k1.left);
		
		zx_setcursorpos(7, 15);
		printf("right: ");
		k1.right = menu_define_key();
		printf("%c", k1.right);

		printCenter(12, "are you ok?");
		printCenter(14, "press \"Y\" to accept");
		printCenter(16, "press any to key to remap");
		start = menu_define_key();
		if (start != 'Y') goto REMAP;
		if (start == 'Y') goto MENU;
	}
	if (start != 'S' ){
		goto SELECTOPTION;
	}


RESET:
	zx_cls();

	zx_asciimode(0);
	char i;
	for (i = 0; i < 32; ++i)
	{
		zx_setcursorpos(0, i);
		printf("%c", 136);

		zx_setcursorpos(23, i);
		printf("%c", 136);
	}		
	
	for (i = 0; i < 24; ++i)
	{
		zx_setcursorpos(i, 0);
		printf("%c", 136);

		zx_setcursorpos(i, 31);
		printf("%c", 136);
	}

	snakeX = 3, snakeY = 13, tailX = 3, size = 3, sleep = 50, tailY = snakeY - size, vy = 1, vx = 0, appleX = 5, appleY = 20, check = 0, nx = 0, ny = 0, onx = 0, ony = 0, mx = 0, r1 = 0, r2 = 0;

	for (i = 1; i < size + 1; i++)
	{
		printBloq(snakeX, snakeY- i);
	}
	printHead(snakeX, snakeY);

	printApple(appleX, appleY);

INITGAME:

	in_Wait(sleep);

	if (in_KeyPressed(in_LookupKey(k1.left)) && vy != 1) { 
		vy = -1;
		vx = 0;
	} else if (in_KeyPressed(in_LookupKey(k1.right)) && vy != -1) {
		vy = 1;
		vx = 0;
	} else if (in_KeyPressed(in_LookupKey(k1.up)) && vx != 1) {
		vy = 0;
		vx = -1;
	} else if (in_KeyPressed(in_LookupKey(k1.down)) && vx != -1) {
		vy = 0;
		vx = 1;
	}
	printBloq(snakeX, snakeY);
	snakeX = snakeX + vx;
	snakeY = snakeY + vy;
	
	check = GetChar(snakeX, snakeY);

	if (check == 136 || check == 128) {

		zx_asciimode(1);
		printCenter(11, "GAME OVER");

		if ((size - 3) > hiscore) {
			hiscore = (size - 3);
			printCenter(13, "NEW HI-SCORE");
		}

		printCenter(15, "press any key to restart");
		zx_asciimode(0);

		fgetc_cons();        // wait for keypress

		goto RESTART;
	}
	
	if (check == 141) {
		size++;
		sleep = sleep - 3;
		if (sleep < 9) sleep = 9;
		
		do {
			appleX = (rand() % 22) + 1;
			appleY = (rand() % 30) + 1;
		} while (GetChar(appleX, appleX) != 0);
			
		printApple(appleX, appleY);
	}
	else 
	{
		deleteBloq(tailX, tailY);
		
		onx = nx, ony = ny;		
		ny = 0;
		nx = 0;
		mx = 0;

		if (GetChar(tailX, tailY - 1) == 128) {
			ny = -1;
			mx++;
		} if (GetChar(tailX, tailY + 1) == 128) {
			ny = 1;
			mx++;
		} if (GetChar(tailX - 1, tailY) == 128) {
			nx = -1;
			mx++;
		} if (GetChar(tailX + 1, tailY) == 128) {
			nx = 1;
			mx++;
		}

		if (mx > 1) {
			r1 = 0, r2 = 0;
			if (GetChar(tailX, (tailY + ny)) == 128) r1++;
			if (GetChar(tailX, (tailY + (ny * 2))) == 128) r1++;
			if (GetChar(tailX, (tailY + (ny * 3))) == 128) r1++;

			if (GetChar((tailX + nx), tailY) == 128) r2++;
			if (GetChar((tailX + (nx * 2)), tailY) == 128) r2++;
			if (GetChar((tailX + (nx * 3)), tailY) == 128) r2++;
			
			if (r1 > r2) nx = 0;
			if (r1 < r2) ny = 0;
			if (r1 == r2) {		
				if (onx == 0) 
				{
					nx = 0;
				}
				else if (ony == 0) 
				{
					ny = 0;
				}
			}
		}
		
		tailX = tailX + nx;
		tailY = tailY + ny;
	}

	printHead(snakeX, snakeY);
	
	goto INITGAME;	

	return 0;
}
