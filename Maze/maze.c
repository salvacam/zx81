//zcc +zx81 -lm -create-app maze.c -o maze

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int in_Wait(uint msec);
unsigned int in_Inkey(void);
uint in_KeyPressed(uint scancode);

typedef enum {eMazeX = 22, eMazeY = 22};
typedef enum {eFloor = 0, eWall = 128, eWall2 = 128};

uchar screen[23][23];


// **************************
// **** Global Variables ****
// **************************


// Use clock funtion to generate random moves.
uchar RandDir(char cSeed) {
    static srand(clock());
	return((rand() % 10*cSeed)/10);
}

// Print char to display File
void SetChar(char cX, char cY, char cValue){
	bpoke(wpeek(16396)+1+cX+cY*33,cValue);
}

void printBorde(uchar pX, uchar pY){
	
	/*
	if (cX - 1 > 0 && cY > 0 && cX - 1 < 23 && cY < 23 ) SetChar(cX - 1, cY, screen[cX-1][cY]);
	if (cX - 1 > 0 && cY - 1 > 0 && cX - 1 < 23 && cY - 1 < 23 ) SetChar(cX - 1, cY - 1, screen[cX-1][cY-1]);
	if (cX - 1 > 0 && cY + 1 > 0 && cX - 1 < 23 && cY + 1 < 23 ) SetChar(cX - 1, cY + 1, screen[cX-1][cY+1]);
	if (cX + 1 > 0 && cY > 0 && cX + 1 < 23 && cY < 23 ) SetChar(cX + 1, cY, screen[cX+1][cY]);
	if (cX + 1 > 0 && cY - 1 > 0 && cX + 1 < 23 && cY - 1 < 23 ) SetChar(cX + 1, cY - 1, screen[cX+1][cY-1]);
	if (cX + 1 > 0 && cY + 1 > 0 && cX + 1 < 23 && cY + 1 < 23 ) SetChar(cX + 1, cY + 1, screen[cX+1][cY+1]);
	if (cX > 0 && cY - 1 > 0 && cX < 23 && cY - 1 < 23 ) SetChar(cX, cY - 1, screen[cX][cY-1]);
	if (cX > 0 && cY + 1 > 0 && cX < 23 && cY + 1 < 23 ) SetChar(cX, cY + 1, screen[cX][cY+1]);
	*/
	SetChar(pX - 1, pY, screen[pX-1][pY]);
	SetChar(pX - 1, pY - 1, screen[pX-1][pY-1]);
	SetChar(pX - 1, pY + 1, screen[pX-1][pY+1]);
	SetChar(pX + 1, pY, screen[pX+1][pY]);
	SetChar(pX + 1, pY - 1, screen[pX+1][pY-1]);
	SetChar(pX + 1, pY + 1, screen[pX+1][pY+1]);
	SetChar(pX, pY - 1, screen[pX][pY-1]);
	SetChar(pX, pY + 1, screen[pX][pY+1]);
}

uchar GetChar(char cX, char cY){
	return wpeek(wpeek(16396)+1+cX+cY*33);
}

void SetDir(char *cX, char *cY){

	char cDir = RandDir(4);

	switch (cDir) {
			// North
			case 0:
				*cY = -1;
				*cX = 0;
			break;

			// East
			case 1:
				*cY = 0;
				*cX = 1;
			break;

			// South
			case 2:
				*cY = 1;
				*cX = 0;
			break;

			// West
			case 3:
				*cY = 0;
				*cX = -1;
			break;
		}

}


// Create Paths : provide starting coordinates
void Walk(char cX, char cY){

	char cXDir, cYDir = 0;
	char cX2, cY2 = 0;
	char bFindPath = 0;
	
	while (bFindPath < 16){
	
		//fgetc_cons();        // wait for keypress

		//SetChar(cX,cY,0);
		//TODO
		screen[cX][cY] = 27; //0

		while (	bFindPath < 16){

			SetDir(&cXDir, &cYDir);

			if (cX + cXDir * 2 <= eMazeX &&
				 cX + cXDir * 2 > eFloor &&
				 cY + cYDir * 2 <= eMazeY && 
				 cY + cYDir * 2 > eFloor )
			{
				if (screen[cX + (cXDir * 2)][cY + (cYDir * 2)] == 128){
					//SetChar(cX + cXDir,cY + cYDir,eFloor);
					//TODO
					screen[cX + cXDir][cY + cYDir] = 27; //0;
					//SetChar(cX + cXDir * 2,cY + cYDir * 2,eFloor);
					//TODO
					screen[cX + (cXDir * 2)][cY + (cYDir * 2)] = 27; //0;
					cX = cX + cXDir * 2;
					cY = cY + cYDir * 2;
					bFindPath = 0;
				} else {
					++bFindPath;
				}
			} else {
				++bFindPath;
			}
		}


		cY2 = 1;

		while(cY2 <= eMazeY){

			cX2 = 1;

			while( cX2 <= eMazeX){

				SetDir(&cXDir, &cYDir);

				if (screen[cX2][cY2] == 128 && 
					cX2 + cXDir > 1 && 
					cY2 + cYDir > 1){

					if (cX2 + cXDir * 2 <= eMazeX && 
						cX2 + cXDir * 2 > eFloor && 
						cY2 + cYDir * 2 <= eMazeY && 
						cY2 + cYDir * 2 > eFloor )
					{
						if (screen[cX2 + cXDir * 2][cY2 + cYDir * 2] == 27) { //0){
							//SetChar(cX2 + cXDir ,cY2 + cYDir ,eFloor);
							//TODO
							screen[cX2 + cXDir][cY2 + cYDir] = 27; //0;
							cX = cX2;
							cY = cY2;
							cX2 = eMazeX;
							cY2 = eMazeY;
							bFindPath = 0;
						}
					}

				}
				cX2+=2;
			}
			cY2+=2;
			++bFindPath;
		}
	}
}


void main(){

RESET:

	char cY = 0;
	char cX = 0;
	char cDir = 0;


	fgetc_cons();        // wait for keypress
	srand(clock());

	//zx_setcursorpos(23,0);
    //printf("01234567890123456789012");
	// print out maze square
	for(cY=0; cY<=eMazeY; cY++){
		for (cX = 0; cX <= eMazeX; cX++){
			SetChar(cX,cY,eWall);
			//SetChar(cX, cY, screen[cX][cY]);
			screen[cX][cY] = 128;

	//		zx_setcursorpos(cX,30);
    //		printf("%d",cX);
		}
	}

	//
	cY = RandDir(eMazeY/2)*2+1;
	cX = RandDir(eMazeX/2)*2+1;

		zx_setcursorpos(1,26);
    	printf("wait");

//TODO slow
	//zx_fast();
	Walk(cX,cY);
	//zx_slow();
	
		zx_setcursorpos(1,26);
    	printf("    ");


	uchar pX = 1;
	uchar pY = 1;
    //TODO empezar desde cualquier esquina
	char cStar = RandDir(4);
	switch (cStar) {
		case 1:
			pX = 1;
			pY = 21;
		break;

		case 2:
			pX = 21;
			pY = 1;
		break;

		case 3:
			pX = 21;
			pY = 21;
		break;
	}

	INITGAME:

	SetChar(pX,pY,139);   
	printBorde(pX,pY);         
	in_Wait(10);
	        
	if (in_KeyPressed(in_LookupKey('O')) && screen[pX - 1][pY] == 27 && pX > 1 ) { 
		SetChar(pX,pY,0); 
		in_Wait(10);
		
		--pX;
		SetChar(pX,pY,139); 
		printBorde(pX,pY);
	}

	if (in_KeyPressed(in_LookupKey('P')) && screen[pX + 1][pY] == 27 && pX < 21) { 
		SetChar(pX,pY,0); 
		
		++pX;
		SetChar(pX,pY,139);  
		printBorde(pX,pY);
	}

	if (in_KeyPressed(in_LookupKey('Q')) && screen[pX][pY - 1] == 27 && pY > 1) { 
		SetChar(pX,pY,0); 
		
		--pY;
		SetChar(pX,pY,139); 
		printBorde(pX,pY); 
	}

	if (in_KeyPressed(in_LookupKey('A')) && screen[pX][pY + 1] == 27 && pY < 21) { 
		SetChar(pX,pY,0); 
		
		++pY;
		SetChar(pX,pY,139);  
		printBorde(pX,pY);
	}

	if (in_KeyPressed(in_LookupKey('R'))) { 
		goto RESET;
	}

	goto INITGAME;
}
