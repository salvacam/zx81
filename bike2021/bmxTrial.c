#include <stdio.h>
#include <stdlib.h>
#include <zx81.h>
void in_Pause(uint msec);
char strlen(const char *s);
uint in_KeyPressed(uint scancode);

uchar GetChar(char cY, char cX)
{
	return wpeek(wpeek(16396) + 1 + cX + cY * 33);
}

void SetChar(char cX, char cY, char cValue) {
	bpoke(wpeek(16396) + 1 + cY + cX * 33, cValue);
}

void printStar(char x, char y) {
	do {
		zx_setcursorpos(x, y);
		zx_asciimode(1);
		printf("PRESS ANY KEY TO START");
		zx_asciimode(0);
		zx_setcursorpos(x, y + 5);
		printf("%c", 128);
		zx_setcursorpos(x, y + 9);
		printf("%c", 128);
		zx_setcursorpos(x, y + 13);
		printf("%c", 128);
		zx_setcursorpos(x, y + 16);
		printf("%c", 128);
		zx_asciimode(1);
		in_Pause(75);
		zx_setcursorpos(x, y);
		printf("press any key to start");
		in_Pause(75);
	} while (getk() == '\0');
}

void printInit() {
	/*
	char scr_addr [24][32] = {
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,133,3,3,3,3,3,128,5,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,131,131,131,0,0,0,0,0,0,0,0,133,0,131,131,131,131,3,5,0,0},
		{0,0,0,0,0,0,0,0,0,87,128,128,128,128,128,4,0,0,0,0,0,0,133,0,3,3,3,3,131,5,0,0},
		{0,0,0,0,0,0,0,0,87,128,128,128,128,128,128,128,4,0,0,0,0,0,133,0,131,131,131,131,3,5,0,0},
		{0,0,0,0,0,0,0,0,129,128,128,128,128,128,128,128,130,0,0,0,0,0,133,0,128,128,128,128,0,5,0,0},
		{0,0,0,0,0,0,0,0,132,5,3,128,7,0,132,128,128,0,0,0,0,0,133,0,3,3,3,3,0,5,0,0},
		{0,0,0,0,0,0,0,0,0,5,4,2,128,5,133,128,7,0,0,0,0,0,133,131,131,131,131,131,128,5,0,0},
		{0,0,0,0,0,0,0,0,133,1,0,0,128,5,129,128,1,0,0,0,0,0,133,3,128,128,128,128,3,5,0,0},
		{0,0,0,0,0,0,0,0,133,0,0,87,128,87,128,128,1,0,0,0,0,0,133,0,3,128,128,3,0,5,0,0},
		{0,0,0,0,0,0,0,0,0,134,131,129,7,3,128,0,0,0,0,0,0,0,133,0,0,3,3,0,0,5,0,0},
		{0,0,0,0,0,0,0,0,0,0,87,128,0,128,128,130,0,0,0,0,0,0,133,0,128,0,0,128,0,5,0,0},
		{0,0,0,0,0,0,0,87,7,3,128,1,129,128,128,128,4,0,0,0,0,0,133,0,128,128,128,128,0,5,0,0},
		{0,0,0,0,0,0,0,129,87,4,0,129,128,128,128,128,5,0,0,0,0,0,133,0,128,128,128,128,0,5,0,0},
		{0,0,0,0,87,131,4,128,130,128,128,3,129,128,128,128,5,0,0,0,0,0,133,131,128,128,128,128,131,5,0,0},
		{0,0,0,129,128,128,128,128,4,0,87,129,128,128,128,128,131,131,0,0,0,0,133,3,128,128,128,128,3,5,0,0},
		{0,0,129,128,128,3,132,128,130,0,128,128,128,128,128,128,128,128,130,4,0,0,133,131,3,128,128,3,131,5,0,0},
		{0,87,128,128,0,0,87,128,128,130,132,128,128,134,128,128,7,132,128,128,0,0,133,128,131,3,3,131,128,5,0,0},
		{0,133,128,1,134,0,129,7,128,128,128,128,128,133,128,134,0,0,129,128,5,0,133,128,128,0,0,128,128,5,0,0},
		{0,133,128,0,0,128,1,0,133,128,131,128,128,128,7,0,134,6,0,128,5,0,133,128,3,131,131,3,128,5,0,0},
		{0,133,128,4,6,0,134,87,129,128,5,133,0,128,130,0,6,134,0,128,5,0,133,3,131,128,128,131,3,5,0,0},
		{0,133,128,128,0,0,0,128,128,7,130,0,87,128,128,6,0,0,132,128,5,0,133,131,128,128,128,128,131,5,0,0},
		{0,0,132,128,128,131,129,128,128,1,2,128,7,2,128,128,131,129,128,7,0,135,131,135,131,0,4,131,4,4,0,0},
		{0,0,0,3,128,128,128,128,1,0,0,0,0,0,2,128,128,128,7,0,0,0,5,133,131,1,5,130,5,5,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,0,1,1,1,1,3,1,0}
	};
	*/

	zx_fast();
	/*
	char i;
	char j;
	for (i = 0; i < 24; i++) {
		for (j = 0; j < 32; j++) {
			bpoke(wpeek(16396) + 1 + j + i * 33, scr_addr[i][j]);
		}
	}
	*/

	zx_asciimode(0);
	zx_setcursorpos(0, 0);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x03, 0x03, 0x03, 0x03, 0x03, 0x80, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x83, 0x83, 0x83, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x83, 0x83, 0x83, 0x83, 0x03, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87, 0x80, 0x80, 0x80, 0x80, 0x80, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x03, 0x03, 0x03, 0x03, 0x83, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x83, 0x83, 0x83, 0x83, 0x03, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x81, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x82, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x80, 0x80, 0x80, 0x80, 0x00, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x84, 0x05, 0x03, 0x80, 0x07, 0x00, 0x84, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x03, 0x03, 0x03, 0x03, 0x00, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x04, 0x02, 0x80, 0x05, 0x85, 0x80, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x83, 0x83, 0x83, 0x83, 0x83, 0x80, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x01, 0x00, 0x00, 0x80, 0x05, 0x81, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x03, 0x80, 0x80, 0x80, 0x80, 0x03, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x00, 0x87, 0x80, 0x87, 0x80, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x03, 0x80, 0x80, 0x03, 0x00, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x86, 0x83, 0x81, 0x07, 0x03, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87, 0x80, 0x00, 0x80, 0x80, 0x82, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x80, 0x00, 0x00, 0x80, 0x00, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87, 0x07, 0x03, 0x80, 0x01, 0x81, 0x80, 0x80, 0x80, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x80, 0x80, 0x80, 0x80, 0x00, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x81, 0x87, 0x04, 0x00, 0x81, 0x80, 0x80, 0x80, 0x80, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x80, 0x80, 0x80, 0x80, 0x00, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x87, 0x83, 0x04, 0x80, 0x82, 0x80, 0x80, 0x03, 0x81, 0x80, 0x80, 0x80, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x83, 0x80, 0x80, 0x80, 0x80, 0x83, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x81, 0x80, 0x80, 0x80, 0x80, 0x04, 0x00, 0x87, 0x81, 0x80, 0x80, 0x80, 0x80, 0x83, 0x83, 0x00, 0x00, 0x00, 0x00, 0x85, 0x03, 0x80, 0x80, 0x80, 0x80, 0x03, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x81, 0x80, 0x80, 0x03, 0x84, 0x80, 0x82, 0x00, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x82, 0x04, 0x00, 0x00, 0x85, 0x83, 0x03, 0x80, 0x80, 0x03, 0x83, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x87, 0x80, 0x80, 0x00, 0x00, 0x87, 0x80, 0x80, 0x82, 0x84, 0x80, 0x80, 0x86, 0x80, 0x80, 0x07, 0x84, 0x80, 0x80, 0x00, 0x00, 0x85, 0x80, 0x83, 0x03, 0x03, 0x83, 0x80, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x85, 0x80, 0x01, 0x86, 0x00, 0x81, 0x07, 0x80, 0x80, 0x80, 0x80, 0x80, 0x85, 0x80, 0x86, 0x00, 0x00, 0x81, 0x80, 0x05, 0x00, 0x85, 0x80, 0x80, 0x00, 0x00, 0x80, 0x80, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x85, 0x80, 0x00, 0x00, 0x80, 0x01, 0x00, 0x85, 0x80, 0x83, 0x80, 0x80, 0x80, 0x07, 0x00, 0x86, 0x06, 0x00, 0x80, 0x05, 0x00, 0x85, 0x80, 0x03, 0x83, 0x83, 0x03, 0x80, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x85, 0x80, 0x04, 0x06, 0x00, 0x86, 0x87, 0x81, 0x80, 0x05, 0x85, 0x00, 0x80, 0x82, 0x00, 0x06, 0x86, 0x00, 0x80, 0x05, 0x00, 0x85, 0x03, 0x83, 0x80, 0x80, 0x83, 0x03, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x85, 0x80, 0x80, 0x00, 0x00, 0x00, 0x80, 0x80, 0x07, 0x82, 0x00, 0x87, 0x80, 0x80, 0x06, 0x00, 0x00, 0x84, 0x80, 0x05, 0x00, 0x85, 0x83, 0x80, 0x80, 0x80, 0x80, 0x83, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x84, 0x80, 0x80, 0x83, 0x81, 0x80, 0x80, 0x01, 0x02, 0x80, 0x07, 0x02, 0x80, 0x80, 0x83, 0x81, 0x80, 0x07, 0x00, 0x87, 0x83, 0x87, 0x83, 0x00, 0x04, 0x83, 0x04, 0x04, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x03, 0x80, 0x80, 0x80, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x80, 0x80, 0x80, 0x07, 0x00, 0x00, 0x00, 0x05, 0x85, 0x83, 0x01, 0x05, 0x82, 0x05, 0x05, 0x00, 0x00);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x01, 0x00);

	zx_asciimode(1);
	zx_slow();
}

void printCenter(char numLiner, char* textCenter) {
	char lenCenter = strlen(textCenter);
	char whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner, whiteSpace);
	printf("%s", textCenter);
}

void printLineClean(char line) {
	bpoke(wpeek(16396) + 1 + 3 + (line * 33), 136);
	bpoke(wpeek(16396) + 1 + 4 + (line * 33), 136);

	bpoke(wpeek(16396) + 1 + 27 + (line * 33), 136);
	bpoke(wpeek(16396) + 1 + 28 + (line * 33), 136);
}

void printLine(char line, char linePrint[4]) {
	bpoke(wpeek(16396) + 1 + 3 + (line * 33), 136);
	bpoke(wpeek(16396) + 1 + 4 + (line * 33), 136);

	char y;
	for (y = 0; y < 4; y++) {
		bpoke(wpeek(16396) + 1 + linePrint[y] + (line * 33), 136);
	}

	bpoke(wpeek(16396) + 1 + 27 + (line * 33), 136);
	bpoke(wpeek(16396) + 1 + 28 + (line * 33), 136);
}

char valueinarray(char val, char array[6]) {
	char y;
	for (y = 0; y < 6; y++) {
		if (array[y] == val) return 1;
	}
	return 0;
}


void scrollBike(void) {
#asm
	ld	hl, (16396)	; D_FILE

	ld	bc, 33 * 20 ; El segundo valor defina las filas que va ha subir
	add	hl, bc
	ld	de, 33
	push hl
	add hl, de
	pop de
	ex de, hl
	lddr
	xor a
	ld b, 32
blankline:
	inc hl
	ld (hl), a
	djnz blankline
	jp zx_topleft

#endasm
}

void printCharacter (char x, char y)
{
	SetChar(x, y + 1, 128);

	SetChar(x + 1, y, 2);
	SetChar(x + 1, y + 1, 137);
	SetChar(x + 1, y + 2, 1);

	SetChar(x + 2, y + 1, 128);
}

void printRock (char y)
{
	SetChar(0, y, 129);
	SetChar(0, y + 1, 130);

	SetChar(1, y, 132);
	SetChar(1, y + 1, 7);
}

void printCyclist(char x, char y)
{
	SetChar(x, y + 1, 8);

	SetChar(x + 1, y, 135);
	SetChar(x + 1, y + 1, 128);
	SetChar(x + 1, y + 2, 4);

	SetChar(x + 2, y + 1, 8);
}

void printCharacterLeft (char x, char y)
{
	SetChar(x, y + 1, 133);
	SetChar(x, y + 2, 5);

	SetChar(x + 1, y, 2);
	SetChar(x + 1, y + 1, 137);
	SetChar(x + 1, y + 2, 3);

	SetChar(x + 2, y + 1, 133);
	SetChar(x + 2, y + 2, 5);
}

void printCharacterRight (char x, char y)
{
	SetChar(x, y, 133);
	SetChar(x, y + 1, 5);

	SetChar(x + 1, y, 3);
	SetChar(x + 1, y + 1, 137);
	SetChar(x + 1, y + 2, 1);

	SetChar(x + 2, y, 133);
	SetChar(x + 2, y + 1, 5);
}

void deleteCharacter (char x, char y, char back[9])
{
	SetChar(x, y, back[0]);
	SetChar(x, y + 1, back[1]);
	SetChar(x, y + 2, back[2]);

	SetChar(x + 1, y, back[3]);
	SetChar(x + 1, y + 1, back[4]);
	SetChar(x + 1, y + 2, back[5]);

	SetChar(x + 2, y, back[6]);
	SetChar(x + 2, y + 1, back[7]);
	SetChar(x + 2, y + 2, back[8]);
}

void deleteChar (char x, char y)
{
	SetChar(x, y, 0);
	SetChar(x, y + 1, 0);
	SetChar(x, y + 2, 0);

	SetChar(x + 1, y, 0);
	SetChar(x + 1, y + 1, 0);
	SetChar(x + 1, y + 2, 0);

	SetChar(x + 2, y, 0);
	SetChar(x + 2, y + 1, 0);
	SetChar(x + 2, y + 2, 0);
}

void printCrash (char x, char y)
{
	SetChar(x, y, 6);
	SetChar(x, y + 1, 138);
	SetChar(x, y + 2, 134);

	SetChar(x + 1, y, 134);
	SetChar(x + 1, y + 1, 137);
	SetChar(x + 1, y + 2, 6);
}

void updateHighScore(int score) {
	if (score < 10) {
		zx_setcursorpos(23, 19);
		printf("%c", 156 + score);
	}
	else if (score < 100) {
		zx_setcursorpos(23, 19);
		printf("%c%c", 156 + (score / 10), 156 + (score % 10));
	}
	else {
		zx_setcursorpos(23, 19);
		printf("%c%c%c", 156 + (score / 100), 156 + ((score / 10) % 10), 156 + (score % 10));
	}
}

void updateScore(int score) {
	if (score < 10) {
		zx_setcursorpos(22, 17);
		printf("%c", 156 + score);
	}
	else if (score < 100) {
		zx_setcursorpos(22, 17);
		printf("%c%c", 156 + (score / 10), 156 + (score % 10));
	}
	else if (score < 1000) {
		zx_setcursorpos(22, 17);
		printf("%c%c%c", 156 + (score / 100), 156 + ((score / 10) % 10), 156 + (score % 10));
	}
	else {
		zx_setcursorpos(22, 17);
		printf("%c%c%c%c", 156 + (score / 1000), 156 + ((score / 100) % 10), 156 + ((score / 10) % 10), 156 + (score % 10));
	}
}

void updateLevel(int level, char *levelRock, char *cicleBike) {
	if (level < 10) {
		*levelRock = 10;
		*cicleBike = 100;
	}
	else if (level > 1000) {
		*levelRock = 1;
		*cicleBike = 3;
	}
	else if (level > 500) {
		*levelRock = 2;
		*cicleBike = 5;
	}
	else if (level > 250) {
		*levelRock = 3;
		*cicleBike = 10;
	}
	else if (level > 99) {
		*levelRock = 4;
		*cicleBike = 15;
	}
	else if (level > 75) {
		*cicleBike = 30;
	}
	else if (level > 50) {
		*levelRock = 6;
		*cicleBike = 45;
	}
	else if (level > 25) {
		*cicleBike = 60;
	}
	else if (level > 9) {
		*levelRock = 8;
		*cicleBike = 75;
	}
}

int main(void)
{
	int hiScore = 0;

	char lineLeft[20][4] = {
		{ 23, 24, 25, 26 },
		{  5, 24, 25, 26 },
		{  5,  6, 25, 26 },
		{  5,  6,  7, 26 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7,  8 },
		{  5,  6,  7, 26 },
		{  5,  6, 25, 26 },
		{  5, 24, 25, 26 },
		{ 23, 24, 25, 26 }
	};

	char lineRight[20][4] = {
		{  5,  6,  7, 26 },
		{  5,  6,  7, 26 },
		{  5,  6, 25, 26 },
		{  5, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{ 23, 24, 25, 26 },
		{  5, 24, 25, 26 },
		{  5,  6, 25, 26 },
		{  5,  6, 25, 26 },
		{  5,  6,  7, 26 }
	};

	char lineUnionLeft[4] = { 5, 6, 27, 28 };

	char a;


INITGAME:

	printInit();
	printStar(0, 0);

	zx_cls();

	char back[9] = {  0,  0,  0, 0,  0,  0,  0,  0,  0};

	for (a = 0; a < 20; ++a)
	{
		printLineClean(a);
	}


	zx_asciimode(1);
	printCenter(21, "BMX trial");
	printCenter(22, "score: 0   ");
	printCenter(23, "hi-score: 0   ");

	printCenter(0, "press SPACE o M to pedal");
	printCenter(2, "O to left, P to right");

	zx_asciimode(0);
	if (hiScore > 0) {
		updateHighScore(hiScore);
	}

	char arr[6] = {7, 129, 130, 132, 128, 8};

	char x = 15;
	char y = 15;

	printCharacter(x, y);

	int cicle = 0;
	char countPrint = 0;
	char countRock = 0;
	char levelRock = 10;
	char yRock;
	char countCyclist = 0;
	char xCyclist = 0;
	char yCyclist = 0;
	char dirPrint = 0;
	char moveV = 0;
	char printCenterVar = 0;
	char out = 0;
	int score = 0;
	char cicleBike = 100;

MARK:

	cicle++;
	if (cicle >= 250) {
		cicle = 1;
	}

	if (cicle % 5 == 0 ) {
		moveV = 0;
	}

	if (in_KeyPressed(in_LookupKey('O')) && y > 3 && moveV != 0) {
		printCenterVar = 1;
		deleteCharacter(x, y, back);

		back[0] = GetChar(x, y - 1);
		back[3] = GetChar(x + 1, y - 1);
		back[6] = GetChar(x + 2, y - 1);

		back[1] = GetChar(x, y);
		back[4] = GetChar(x + 1, y);
		back[7] = GetChar(x + 2, y);

		back[2] = GetChar(x, y + 1);
		back[5] = GetChar(x + 1, y + 1);
		back[8] = GetChar(x + 2, y + 1);

		y--;

		if ( valueinarray(GetChar(x, y + 1), arr) == 1 || valueinarray(GetChar(x + 1, y + 1), arr) == 1 || valueinarray(GetChar(x + 2, y + 1), arr) == 1 ) {
			printCrash(x, y);
			goto REBOOT;
		}
		printCharacterLeft(x, y);
	}

	if (in_KeyPressed(in_LookupKey('P')) && y < 26 && moveV != 0) {
		printCenterVar = 1;
		deleteCharacter(x, y, back);

		back[0] = GetChar(x, y + 1);
		back[3] = GetChar(x + 1, y + 1);
		back[6] = GetChar(x + 2, y + 1);

		back[1] = GetChar(x, y + 2);
		back[4] = GetChar(x + 1, y + 2);
		back[7] = GetChar(x + 2, y + 2);

		back[2] = GetChar(x, y + 3);
		back[5] = GetChar(x + 1, y + 3);
		back[8] = GetChar(x + 2, y + 3);

		y++;

		if ( valueinarray(GetChar(x, y + 1), arr) == 1 || valueinarray(GetChar(x + 1, y + 1), arr) == 1 || valueinarray(GetChar(x + 2, y + 1), arr) == 1 ) {
			printCrash(x, y);
			goto REBOOT;

		}
		printCharacterRight(x, y);
	}

	if (in_KeyPressed(in_LookupKey('M')) || in_KeyPressed(in_LookupKey(' ')) ) {
		moveV = 1;

		deleteCharacter(x, y, back);

		scrollBike();

		if (GetChar(x - 1, y + 1) == 136) {
			out = 1;
		} else {
			out = 0;
		}
		if (x > 10 && out == 0) {
			x--;
		} else {
			if (x < 17) {
				x++;
			}
		}

		if ( valueinarray(GetChar(x, y + 1), arr) == 1 || valueinarray(GetChar(x + 1, y + 1), arr) == 1 || valueinarray(GetChar(x + 2, y + 1), arr) == 1 ) {
			printCrash(x, y);
			goto REBOOT;
		}

		back[0] = GetChar(x, y);
		back[1] = GetChar(x, y + 1);
		back[2] = GetChar(x, y + 2);

		back[3] = GetChar(x + 1, y);
		back[4] = GetChar(x + 1, y + 1);
		back[5] = GetChar(x + 1, y + 2);

		back[6] = GetChar(x + 2, y);
		back[7] = GetChar(x + 2, y + 1);
		back[8] = GetChar(x + 2, y + 2);

		printCharacter(x, y);

		if (countPrint < 19) {
			if (dirPrint == 0)
			{
				printLine(0, lineLeft[countPrint]);
			}
			else
			{
				printLine(0, lineRight[countPrint]);
			}
			countPrint++;
		} else {
			if (dirPrint == 0) {
				dirPrint = 1;
			} else {
				dirPrint = 0;
			}
			countPrint = 0;
			printLine(0, lineUnionLeft);
		}

		countRock++;
		if (countRock >= levelRock)
		{
			if (score < 9999)
			{
				score++;
				updateScore(score);
				updateLevel(score, &levelRock, &cicleBike);
			}

			countRock = 0;
			yRock = rand() % 23 + 4;
			printRock(yRock);
		}

		countCyclist++;
		if (countCyclist == cicleBike) {
			if (score < 9995) 
			{
				score = score + 5;
				updateScore(score);
				updateLevel(score, &levelRock, &cicleBike);
			}
			yCyclist = rand() % 22 + 4;
		}
		if (countCyclist >= cicleBike && countCyclist % 10 == 0) {
			deleteChar(xCyclist, yCyclist);
			xCyclist = xCyclist + 2;
			if (rand () % 2 == 0) {
				if (yCyclist < 25)
				{
					yCyclist++;
				}
				else
				{
					yCyclist--;
				}
			} else {
				if (yCyclist > 5)
				{
					yCyclist--;
				}
				else
				{
					yCyclist++;
				}
			}
			if (xCyclist < 18 && valueinarray(GetChar(xCyclist, yCyclist + 1), arr) == 0 &&
			        valueinarray(GetChar(xCyclist, yCyclist), arr) == 0 && valueinarray(GetChar(xCyclist, yCyclist + 2), arr) == 0 &&
			        valueinarray(GetChar(xCyclist + 1, yCyclist + 1), arr) == 0 &&
			        valueinarray(GetChar(xCyclist + 1, yCyclist), arr) == 0 && valueinarray(GetChar(xCyclist + 1, yCyclist + 2), arr) == 0 &&
			        valueinarray(GetChar(xCyclist + 2, yCyclist + 1), arr) == 0 &&
			        valueinarray(GetChar(xCyclist + 2, yCyclist), arr) == 0 && valueinarray(GetChar(xCyclist + 2, yCyclist + 2), arr) == 0) {
				printCyclist(xCyclist, yCyclist);
			} else {
				countCyclist = 0;
				xCyclist = 0;
			}
		}
	}

	countCyclist++;

	if (countCyclist == cicleBike) {
		if (moveV == 1 && score < 9995) 
		{
			score = score + 5;
			updateScore(score);
			updateLevel(score, &levelRock, &cicleBike);
		}
		deleteChar(xCyclist, yCyclist);
		yCyclist = rand() % 20 + 5;
	} else if (countCyclist >= cicleBike) {
		deleteChar(xCyclist, yCyclist);

		if (moveV == 1) {
			xCyclist = xCyclist + 2;
		} else {
			xCyclist++;
		}

		if (rand () % 2 == 0) {
			if (yCyclist < 25)
			{
				yCyclist++;
			}
			else
			{
				yCyclist--;
			}
		} else {
			if (yCyclist > 5)
			{
				yCyclist--;
			}
			else
			{
				yCyclist++;
			}
		}
		if ( (xCyclist == x && yCyclist == y) || (xCyclist + 1  == x && yCyclist == y) || (xCyclist + 2 == x && yCyclist == y) ||
		        (xCyclist == x + 1 && yCyclist == y) || (xCyclist + 1  == x + 1 && yCyclist == y) || (xCyclist + 2 == x + 1 && yCyclist == y) ||
		        (xCyclist == x + 2 && yCyclist == y) || (xCyclist + 1  == x + 2 && yCyclist == y) || (xCyclist + 2 == x + 2 && yCyclist == y) ) {
			printCrash(x, y);

			goto REBOOT;
		}

		if (xCyclist < 17 && valueinarray(GetChar(xCyclist, yCyclist + 1), arr) == 0 &&
		        valueinarray(GetChar(xCyclist + 1, yCyclist + 1), arr) == 0 &&
		        valueinarray(GetChar(xCyclist + 2, yCyclist + 1), arr) == 0) {
			printCyclist(xCyclist, yCyclist);
		}
		else
		{
			deleteChar(xCyclist, yCyclist);
			countCyclist = 0;
			xCyclist = -1;
		}
	}

	if (printCenterVar == 1 && cicle % 30 == 0 ) {
		printCenterVar = 0;
		deleteChar(x, y);
		printCharacter(x, y);
	}

	goto MARK;


REBOOT:

	zx_asciimode(1);
	if (score > hiScore ) {
		hiScore = score;
		printCenter(12, "NEW HI-SCORE");
	}

	fgetc_cons();        // wait for keypress
	printStar(10, 5);
	zx_asciimode(0);

	goto INITGAME;

	return 0;
}