' zx81imgtxt2bin
' Convierte a asm (para buffer de la zx81mtlib)
' hecho en la utilidad online zxpaintyone

' C�digo cutre ahead.

Dim as String myLine
Dim as String myHex 
Dim as uByte myChar 
Dim as Integer i, j
Dim as String myOutput

Print "zx81imgtxt2bin - Conversor de zxpaintyone a asm (para buffer de la zx81mtlib)"
Print "USO: zx81imgtxt2bin imagen"
Print "imagen debe ser un archivo.txt. No especificad el txt en el comando"
Print "Por ejemplo: "
Print "zx81imgtxt2bin title"
Print "Convertir� title.txt a title.asm (para buffer de la zx81mtlib)"

If Command = "" Then End

open Command + ".txt" For Input as #1
open Command + ".c" For Output as #2


' Now output 24 32 character lines with a trailing 118

for i = 1 to 24
	input #1, myLine
	myOutput = "printf(""%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c"", "
	for j = 0 to 31
		myHex = mid(myLine, 1 + j + j , 2)
        myOutput = myOutput + "0x" + myHex

        if (j< 31) then
        	myOutput = myOutput + ", "
		Else
        	myOutput = myOutput + "); "
		End If		
	next j	
	print #2, myOutput
next i

Close
