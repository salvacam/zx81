Dim bitmap(47,63) As Byte
Dim i As Integer, x As Integer, y As Integer
Dim generated As String
Dim inFile As String
Dim outFile As String
Dim numLine As String
Dim ink As Short, paper As Short, cInk As Short, cPaper As Short
Dim c(3) As Short
Dim numColores As Short
Dim masRepetido As Short
Dim masRepetido2 As Short
Dim c1 As Short
Dim c2 As Short
Dim patron As Byte
Dim basInPatron As String
Dim zx81Code As String
Dim as String myOutput

Function cuentaColores (c () As Short) As Short
	Dim i As Short
	Dim j As Short
	Dim res As Short
	Dim found As Short
	Dim cnt (3) As Short

	res = 0
	cnt (res) = c(0)

	For i = 1 to 3
		found = 0
		For j = 0 To res
			If c(i) = cnt (j) Then
				found = -1
				Exit For
			End If
		Next j
		If Not found Then
			res = res + 1
			cnt (res) = c(i)
		End If
	Next i

	cuentaColores = res + 1
End Function

Print "No voy a hacer ninguna comprobaci�n de errores, as� que si te equivoca lacagao"
' inFile = command

If Command = "" Then End

open Command + ".raw" For Input as #1
open Command + ".txt" For Output as #2

Print "Leyendo archivo de entrada..."

' Abro binariamente el archivo RAW de 64x48 y meto los pixels en un array
' Open inFile For Binary As #1
For y = 0 To 47
	For x = 0 To 63
		Get #1, , bitmap (y, x)
Next x, y
Close #1

Print "Generando patrones de caracteres..."
ink = 0
paper = 7

cInk = -1
cPaper = -1


' Recorro el array cogiendo chunks de 2x2 pixels y busco dos colores:
For y = 0 To 23
	generated = ""
	For x = 0 To 31

		c(1) = bitmap (2 * y, 2 * x)
		c(0) = bitmap (2 * y, 2 * x + 1)
		c(3) = bitmap (2 * y + 1, 2 * x)
		c(2) = bitmap (2 * y + 1, 2 * x + 1)

		' Buscamos el patr�n
		patron = 0
		For i = 0 To 3
			If c(i) = 0 Then patron = patron + 2 ^ i 
		Next i

		' Generamos el patron BasIN:

		Select Case patron
			Case 0:	basInPatron = "  ": zx81Code = "0"
			Case 1:	basInPatron = " '": zx81Code = "2"
			Case 2:	basInPatron = "' ": zx81Code = "1"
			Case 3:	basInPatron = "''": zx81Code = "3"
			Case 4:	basInPatron = " .": zx81Code = "135"
			Case 5: basInPatron = " :":	zx81Code = "133"
			Case 6: basInPatron = "'.": zx81Code = "134"
			Case 7: basInPatron = "':": zx81Code = "132"
			Case 8: basInPatron = ". ": zx81Code = "4"
			Case 9: basInPatron = ".'": zx81Code = "6"
			Case 10:basInPatron = ": ": zx81Code = "5"
			Case 11:basInPatron = ":'": zx81Code = "7"
			Case 12:basInPatron = "..": zx81Code = "131"
			Case 13:basInPatron = ".:": zx81Code = "129"
			Case 14:basInPatron = ":.": zx81Code = "130"
			Case 15:basInPatron = "::": zx81Code = "128"
		End Select

		'if Command$ <> "" then Print numColores, c1, c2, basInPatron

		' Output patron
		if (x = 0) then generated = generated + "{"
		generated = generated + zx81Code

		if (x < 31) then generated = generated + ", "
		if (x = 31) then generated = generated + "},"

	Next x
	print #2, generated
Next y
close
