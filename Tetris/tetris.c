#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
void in_Pause(uint msec);
char strlen(const char *s);
uint in_KeyPressed(uint scancode);
uint b(uint scancode);
int in_Wait(uint msec);
unsigned int in_Inkey(void);
void in_WaitForKey(void);
void in_WaitForNoKey(void);

int in_InKey(const char *s);

extern int d_file @16396;

int levels [10][9] = {
	{  0,  0,  0,  0,  0,585,585,585,585},
	{513,513,771,771,771,903,903,975,975},
	{ 48, 48, 48, 48, 48, 48, 48, 48, 48},
	{  0,  0,  0, 48, 48, 72,204,462,513},
	{  0,258, 72,  0, 48,  0,132,258,513},
	{  0,  0,  0, 72, 48, 72,645,258,645},
	{  0,120, 72, 72,120,  0,903,645,903},
	{  0, 48, 48,120,204,204,510,771,771},
	{658,658,658,293,293,293,658,658,658},
	{112, 76, 76,112,112,112,112,510,256}
};

// I,J,L,O,S,T,Z
char pieces [7][4][3] = {
	{
		{ 0,15, 0},
		{ 4, 4, 4},
		{ 0, 0,15},
		{ 2, 2, 2}
	},
	{
		{ 8,14, 0},
		{ 6, 4, 4},
		{ 0,14, 2},
		{ 4, 4,12}
	},
	{
		{ 2,14, 0},
		{ 4, 4, 6},
		{ 0,14, 8},
		{12, 4, 4}
	},
	{
		{ 6, 6, 0},
		{ 6, 6, 0},
		{ 6, 6, 0},
		{ 6, 6, 0}
	},
	{
		{ 6,12, 0},
		{ 4, 6, 2},
		{ 0, 6,12},
		{ 8,12, 4}
	},
	{
		{ 4,14, 0},
		{ 4, 6, 4},
		{ 0,14, 4},
		{ 4,12, 4}
	},
	{
		{12, 6, 0},
		{ 2, 6, 4},
		{ 0,12, 6},
		{ 4,12, 8}
	}
};

unsigned char xpos [];
unsigned char ypos [];
char i = 0, j = 0, h = 0;

uchar GetChar(char cX, char cY)
{
	if (cX < 0 || cX > 19) return 1;
	if (cY < 2 || cY > 11) return 1;
	return wpeek(wpeek(16396) + 1 + cY + cX * 33);
}

#asm
	._xpos
		defb 0
	._ypos
		defb 0
#endasm

void SetBlock(unsigned char x, unsigned char y)
{
	xpos [0] = x;
	ypos [0] = y+1;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzer

	ld b, a
	ld de, 33

	.z81tpl
	add	hl, de
	djnz z81tpl				; hl = buffer + 32 * x

   .z81tpzer
    ld a, (_ypos)
    add hl, a;

	ld b, 177
	ld (hl), b

	#endasm
}

void DeleteBlock(unsigned char x, unsigned char y)
{
	xpos [0] = x;
	ypos [0] = y+1;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzer1

	ld b, a
	ld de, 33

	.z81tpl1
	add	hl, de
	djnz z81tpl1				; hl = buffer + 32 * x

   .z81tpzer1
    ld a, (_ypos)
    add hl, a;

	ld b, 14
	ld (hl), b

	#endasm
}

void deletePiece(char currentPiece, unsigned char file, unsigned char col, char rotate) {

	for (i = 0; i < 3; i++) {
		j = pieces[currentPiece][rotate][i];

		if (j >= 8) {
			j = j - 8;
			DeleteBlock(file+i, col);
		}
		if (j >= 4) {
			j = j - 4;
			DeleteBlock(file+i, col+1);
		}
		if (j >= 2) {
			j = j - 2;
			DeleteBlock(file+i, col+2);
		}
		if (j >= 1) {
			j = j - 1;
			DeleteBlock(file+i, col+3);	
		}
	}
	if (currentPiece == 0 && (rotate == 1 || rotate == 3)) {
		if (rotate == 1) {
			DeleteBlock(file+3, col+1);
		}
		else {
			DeleteBlock(file+3, col+2);
		}
	}
}

void printPiece(char currentPiece, unsigned char file, unsigned char col, char rotate) {
	for (i = 0; i < 3; i++) {
		j = pieces[currentPiece][rotate][i];

		if (j >= 8) {
			j = j - 8;
			SetBlock(file+i, col);
		}
		if (j >= 4) {
			j = j - 4;
			SetBlock(file+i, col+1);	
		}
		if (j >= 2) {
			j = j - 2;
			SetBlock(file+i, col+2);	
		}
		if (j >= 1) {
			j = j - 1;
			SetBlock(file+i, col+3,);	
		}
	}
	if (currentPiece == 0 && (rotate == 1 || rotate == 3)) {
		if (rotate == 1) {
			SetBlock(file+3, col+1);
		}
		else {
			SetBlock(file+3, col+2);
		}
	}
}

char checkPiece(char currentPiece, unsigned char file, unsigned char col, char rotate) {
	char free = 1;

	for (i = 0; i < 3; i++) {
		j = pieces[currentPiece][rotate][i];

		if (j >= 8) {
			j = j - 8;
			if (GetChar(file+i, col) != 14) free = 0;
		}
		if (j >= 4) {
			j = j - 4;
			if (GetChar(file+i, col+1) != 14) free = 0;
		}
		if (j >= 2) {
			j = j - 2;
			if (GetChar(file+i, col+2) != 14) free = 0;
		}
		if (j >= 1) {
			j = j - 1;			
			if (GetChar(file+i, col+3) != 14) free = 0;
		}
	}
	if (currentPiece == 0 && (rotate == 1 || rotate == 3)) {
		if (rotate == 1) {			
			if (GetChar(file+3, col+1) != 14) free = 0;
		}
		else {
			if (GetChar(file+3, col+2) != 14) free = 0;
		}
	}

	return free;
}

struct controls {
	unsigned char left;
	unsigned char right;
	unsigned char down;
	unsigned char rotate;
	unsigned char pause;
} k1;

unsigned char start;

unsigned char menu_define_key() {
	unsigned char tmp1;
    in_WaitForKey();
    tmp1 = in_Inkey();
    in_WaitForNoKey();
    return tmp1;
}

void printCenter(char numLiner, char* textCenter) {
	char lenCenter = strlen(textCenter);
	char whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner, whiteSpace);
	printf("%s", textCenter);
}

int main(void)
{
	k1.left = 'O';
	k1.right = 'P';
	k1.down = 'A';
	k1.rotate = 'M';
	k1.pause = 'H';

	unsigned char file = 0, col = 0;

	char currentPiece = 0, printNextPiece = 0, nextPiece = 0, rotate = 0, level = 0, upLevel = 0, isDown = 0, counter = 0;
    unsigned char sleep = 15, cicleDown = 0, cicleRotate = 0, cicleMove = 0, arcade = 0, pause = 0, wait = 150;
    unsigned int hiscore = 0, hiscoreB = 0, lines = 0;

RESTART:
	
	level = 0;
	pause = 0;
	MENU:
	zx_cls();
	zx_asciimode(1);
	printCenter(3, "81-TRIS");
	zx_asciimode(0);
	zx_setcursorpos(2, 11);
	printf("%c%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128,128,128);
	zx_setcursorpos(3, 11);
	printf("%c", 128);
	zx_setcursorpos(3, 12);
	printf("%c%c", 164,157);
	zx_setcursorpos(3, 14);
	printf("%c", 150);
	zx_setcursorpos(3, 19);
	printf("%c", 128);
	zx_setcursorpos(4, 11);
	printf("%c%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128,128,128);
	zx_asciimode(1);
	printCenter(8, "press \"R\" to redefine keys");
	printCenter(10, "press \"S\" to start mode normal");
	printCenter(12, "press \"A\" to start mode arcade");

	zx_setcursorpos(15, 5);
	printf("left:  %c", k1.left);
		
	zx_setcursorpos(16, 5);
	printf("right: %c", k1.right);

	zx_setcursorpos(15, 16);
	printf("rotate: %c", k1.rotate);

	zx_setcursorpos(16, 16);
	printf("down:   %c", k1.down);

	zx_setcursorpos(17, 16);
	printf("pause:  %c", k1.pause);

	if (hiscore > 0) {
		zx_setcursorpos(20, 5);
		printf("normal hi-lines: %d", hiscore);
	}

	if (hiscoreB > 0) {
		zx_setcursorpos(21, 5);
		printf("arcade hi-lines: %d", hiscoreB);
	}

	printCenter(23, "2023 salvacam");

SELECTOPTION:

	start = menu_define_key();

	if (start == 'S') {
		arcade = 0;
	}
	if (start == 'A') {
		arcade = 1;
	}

	if (start == 'R') {
		REMAP:
		zx_cls();

		zx_setcursorpos(5, 5);
		printf("left:  ");
		k1.left = menu_define_key();
		printf("%c", k1.left);
			
		zx_setcursorpos(7, 5);
		printf("right: ");
		k1.right = menu_define_key();
		printf("%c", k1.right);

		zx_setcursorpos(5, 15);
		printf("rotate:   ");
		k1.rotate = menu_define_key();
		printf("%c", k1.rotate);
		
		zx_setcursorpos(7, 15);
		printf("down: ");
		k1.down = menu_define_key();
		printf("%c", k1.down);
		
		zx_setcursorpos(9, 5);
		printf("pause: ");
		k1.pause = menu_define_key();
		printf("%c", k1.pause);

		printCenter(13, "are you ok?");
		printCenter(15, "press \"Y\" to accept");
		printCenter(17, "press any to key to remap");
		start = menu_define_key();
		if (start != 'Y') goto REMAP;
		if (start == 'Y') goto MENU;
	}

	if (start != 'S' && start != 'A' ){
		goto SELECTOPTION;
	}

SELECTLEVELDEBUG:

    zx_cls();
    
    if (arcade == 1) {
    	printCenter(5,"game mode arcade");
	} else {
    	printCenter(5,"game mode normal");
	}

    printCenter(7, "select level");
    printCenter(12, "0 1 2 3 4 5 6 7 8 9");

    zx_setcursorpos(16, 6);
    printf("press %c to decrease", k1.left);
        
    zx_setcursorpos(18, 6);
    printf("press %c to increase", k1.right);

    zx_setcursorpos(20, 6);
    printf("press %c to select", k1.rotate);

    zx_asciimode(0);
    zx_setcursorpos(13, (level * 2) + 6);
    printf("%c", 136);

SELECTLEVEL:

    if (in_KeyPressed(in_LookupKey(k1.left)) && level > 0) { 
        zx_setcursorpos(13, (level * 2) + 6);
        printf("%c", 0);
        in_Wait(50);
        level--;
        zx_setcursorpos(13, (level * 2) + 6);
        printf("%c", 136);
    } else if (in_KeyPressed(in_LookupKey(k1.right)) && level < 9) {
        zx_setcursorpos(13, (level * 2) + 6);
        printf("%c", 0);
        in_Wait(50);
        level++;
        zx_setcursorpos(13, (level * 2) + 6);
        printf("%c", 136);
    } else if (in_KeyPressed(in_LookupKey(k1.rotate))) {
        goto RESET;
    } 

    goto SELECTLEVEL;

	srand(clock());

RESET:
	zx_cls();

    rotate = 0, sleep = 15, printNextPiece = 0, file = 0, col = 0, lines = 0, upLevel = 0, nextPiece = rand() % 7, wait = 150;
	zx_asciimode(0);

	for (i = 0; i < 22; ++i)
	{
		zx_setcursorpos(i, 0);
		printf("%c%c", 136,128);

		zx_setcursorpos(i, 12);
		printf("%c%c", 128,136);
	}

	for (i = 2; i < 12; ++i)
	{
		zx_setcursorpos(20, i);
		printf("%c", 128);

		zx_setcursorpos(21, i);
		printf("%c", 136);
	}	

	zx_setcursorpos(21, 1);
	printf("%c", 136);

	zx_setcursorpos(21, 12);
	printf("%c", 136);

	for (i = 1; i < 22; ++i)
	{
		zx_setcursorpos(i, 17);
		if (i > 14 && i < 19) {
			printf("%c%c%c%c%c%c%c%c", 128,  0,  0,  0,  0,  0,  0,128);
		} else {
			printf("%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128);
		}
	}

	zx_setcursorpos(0, 18);
	printf("%c%c%c%c%c%c%c%c", 136,136,136,136,136,136,136,136);

	for (i = 1; i < 22; ++i)
	{
		zx_setcursorpos(i, 25);
		printf("%c", 136);
	}

	zx_asciimode(1);
	zx_setcursorpos( 3, 19);
	printf("LEVEL");
	zx_setcursorpos( 5, 21);
    printf("0%d", level);
	zx_setcursorpos( 8, 19);
	printf("LINES");
	zx_setcursorpos(10, 20);
    printf("00%d", lines);

	zx_setcursorpos(13, 19);
    printf("NEXT", lines);

RESETSCREEN:
	for (i=0; i < 20; i++){
    	for (j=0; j < 10; j++) {
            DeleteBlock(i, j+2);
        }
    }

    int ha = 0;

    if (arcade == 1) {
		for (j=0; j < 9; j++) {
			ha = levels[(level%10)][j];
			h = file+j+11;
			if (level > 9) h--;
			if (level > 19) h--;
			if (level > 29) h--;
			if (level > 39) h--;
			if (level > 49) h--;
    		if (ha >= 512) {
				ha = ha - 512;
				SetBlock(h, col+2);
			}
    		if (ha >= 256) {
				ha = ha - 256;
				SetBlock(h, col+3);
			}
    		if (ha >= 128) {
				ha = ha - 128;
				SetBlock(h, col+4);
			}
    		if (ha >= 64) {
				ha = ha - 64;
				SetBlock(h, col+5);
			}
    		if (ha >= 32) {
				ha = ha - 32;
				SetBlock(h, col+6);
			}
    		if (ha >= 16) {
				ha = ha - 16;
				SetBlock(h, col+7);
			}
    		if (ha >= 8) {
				ha = ha - 8;
				SetBlock(h, col+8);
			}
    		if (ha >= 4) {
				ha = ha - 4;
				SetBlock(h, col+9);
			}
    		if (ha >= 2) {
				ha = ha - 2;
				SetBlock(h, col+10);
			}
    		if (ha >= 1) {
				ha = ha - 1;
				SetBlock(h, col+11);
			}
    	}
    }


INITGAME:

    if (printNextPiece == 0) {        
        cicleDown = 0;
        cicleRotate = 0;
        cicleMove = 0;
        currentPiece = nextPiece;

        for (i = 5; i < 8; ++i)
        {
            if (GetChar(0, i) != 14 || GetChar(1, i) != 14) {
                zx_cls();
                zx_asciimode(1);
                printCenter(12, "game over");
                zx_asciimode(0);
                in_Wait(100);
                fgetc_cons();        // wait for keypress
                goto RESTART;
            }
        }

        for (i = 19; i >= 0; --i)
        {
            counter = 0;
            for (j = 0; j < 10; ++j)
            {
                if (GetChar(i, j+2) != 14) {
                    counter++;
                } else {
                	break;
                }
            }

            if (counter == 10) {
                lines++;
                //lines = lines + 9; // TODO debug
                upLevel++;
                //upLevel = upLevel + 9; // TODO debug
                if (arcade == 1) {
                	if (lines > hiscoreB) hiscoreB = lines;            
                } else {
                	if (lines > hiscore) hiscore = lines;
                }

                for (h = i; h > 0; --h)
                {
                    for (j = 0; j < 10; ++j)
                    {   
                        if (GetChar(h-1, j+2) == 14) {
                        	DeleteBlock(h, j+2);
                        } else {
                        	SetBlock(h, j+2);
                        }
                        //if (GetChar(h-1, j+2) != 14) SetBlock(h, j+2);
                        DeleteBlock(0, j+2);
                    }
                }
                
                zx_asciimode(1);
                zx_setcursorpos(10, 20);
                if (lines < 10) {
                    printf("00%d", lines);
                } else if (lines > 9 && lines < 100) {
                    printf("0%d", lines);
                } else if (lines < 1000) {
                    printf("%d", lines);
                } else {
                    lines = 999;
                    printf("%d", lines);
                }
                zx_asciimode(0);

                if (lines % 10 == 0 && upLevel == 10) { 
                    upLevel = 0;
                    level++;
                    zx_asciimode(1);
                    zx_setcursorpos(5, 21);
                    if (level < 10) {
                        printf("0%d", level);
                    } else if (level > 9 && level < 100) {
                        printf("%d", level);
                    } else {
                        level = 99;
                        printf("%d", level);
                    }
                    zx_asciimode(0);
                    if (arcade == 1) {
						printNextPiece = 0;
						col = 0;
						file = 0;
						for (i=0; i < 20; i++){
    						for (j=0; j < 10; j++) {
            					SetBlock(i, j+2);
            					in_Wait(1);
        					}
            				in_Wait(5);
    					}
                    	goto RESETSCREEN;
                    }
                }

                i++;
            }
        }

        printNextPiece = 1;
        nextPiece = rand() % 7;
        
        zx_asciimode(1);
        zx_setcursorpos(16, 18);
        printf("      ");
        zx_setcursorpos(17, 18);
        printf("      ");
        zx_setcursorpos(18, 18);
        printf("      ");
        zx_asciimode(0);

		printPiece(nextPiece, 16, 19, 0);

        col = 5;
        if (currentPiece == 1 || currentPiece == 4) {
        	col = 6;
        }
        rotate = 0;
		printPiece(currentPiece, file, col, rotate);
    }

    isDown = 0;
    if (pause == 0) {

	    cicleMove++;
	    if (cicleMove > (50 - (level * 3)) || level > 14 ) {
	        if (in_KeyPressed(in_LookupKey(k1.left)) && col > 0) { 
	        	cicleMove = 0;
				deletePiece(currentPiece, file, col, rotate);
		        if (file <= 18 && checkPiece(currentPiece, file, col-1, rotate) == 1) {
		            col--;
				}
	     		printPiece(currentPiece, file, col, rotate);
	        } 
	        else if (in_KeyPressed(in_LookupKey(k1.right)) && col < 10 ) {
	        	cicleMove = 0;
				deletePiece(currentPiece, file, col, rotate);
		        if (file <= 18 && checkPiece(currentPiece, file, col+1, rotate) == 1) {
		            col++;
				}
	     		printPiece(currentPiece, file, col, rotate);
	        }
    	}
		
		// TODO
	    cicleRotate++; 
	    if (cicleRotate > (50 - (level * 2)) || level > 14 ) {
	        if (in_KeyPressed(in_LookupKey(k1.rotate))) {
	        	cicleRotate = 0;
				deletePiece(currentPiece, file, col, rotate);
		        if (file <= 18 && checkPiece(currentPiece, file, col, rotate+1) == 1) {
		            rotate++;
		            if (rotate > 3) rotate = 0;
				}
	     		printPiece(currentPiece, file, col, rotate);
	        }
    	}

	    if (in_KeyPressed(in_LookupKey(k1.down))) {
            in_Wait(10);
		    isDown = 1;
	   		deletePiece(currentPiece, file, col, rotate);
	        if (file <= 18 && checkPiece(currentPiece, file+1, col, rotate) == 1) {
	            file++;
				printPiece(currentPiece, file, col, rotate);
			}
			else {
	     		printPiece(currentPiece, file, col, rotate);
				printNextPiece = 0;
				file = 0;
			}
		}  

	    cicleDown++; 
	    if (cicleDown > (wait - (level * 10)) || level > 15 ) {
	        cicleDown = 0;
	        if (isDown == 0) {
	            in_Wait(sleep);            
		   		deletePiece(currentPiece, file, col, rotate);
		        if (file <= 18 && checkPiece(currentPiece, file+1, col, rotate) == 1) {
		            file++;
					printPiece(currentPiece, file, col, rotate);
				}
				else {
		     		printPiece(currentPiece, file, col, rotate);
					printNextPiece = 0;
					file = 0;
				}
			} else {
	            isDown = 1;
	    	}
		}
	}

    if (in_KeyPressed(in_LookupKey(k1.pause)) && pause == 0) {
	    pause = 1;
		zx_asciimode(1);
		zx_setcursorpos(20, 19);
		printf("PAUSE");
		zx_asciimode(0);
        in_Wait(50);
	} else if (in_KeyPressed(in_LookupKey(k1.pause)) && pause == 1) {
        in_Wait(50);
	    pause = 0;
		zx_setcursorpos(20, 19);
		printf("%c%c%c%c%c", 128,128,128,128,128);
	}  

    goto INITGAME;  

	return 0;
}
