@menu:
FAST
#
print at 3,6;" "; at 3,25;" "; at 10,13;" "; at 17,6;" "; at 17,25;" ";
print at 0,12;"%u%m%u%l% %g%o%n%u";
print at 2,7;"\:'\''\''\':";at 2,21;"\:'\''\''\':";
print at 3,7;"\:   \ :";at 3,21;"\:   \ :";
print at 4,7;"\:   \ :";at 4,21;"\:   \ :";
print at 5,7;"\:.\..\..\.:";at 5,21;"\:.\..\..\.:";
#
print at 9,14;"\:'\''\''\':";
print at 10,14;"\:   \ :";
print at 11,14;"\:   \ :";
print at 12,14;"\:.\..\..\.:";
#
print at 16,7;"\:'\''\''\':";at 16,21;"\:'\''\''\':";
print at 17,7;"\:   \ :";at 17,21;"\:   \ :";
print at 18,7;"\:   \ :";at 18,21;"\:   \ :";
print at 19,7;"\:.\..\..\.:";at 19,21;"\:.\..\..\.:";
#
FOR I=0 TO 9
print at 6+i,7+1;"\ :\: ";at 6+i,22;"\ :\: ";
NEXT I
print at 17,11;"\..\..\..\..\..\..\..\..\..\..";
print at 18,11;"\''\''\''\''\''\''\''\''\''\''";
print at 6,11;"\'.";at 7,12;"\'.";at 7+1,13;"\'.";
print at 6,20;"\.'";at 7,19;"\.'";at 7+1,18;"\.'";
print at 13,13;"\.'";at 14,12;"\.'";at 15,11;"\.'";
print at 13,18;"\'.";at 14,19;"\'.";at 15,20;"\'.";
#
print at 20,0;"press 1 to one player, 2 to two player or i for instructions";
slow
@KEY:
IF INKEY$ = "1" THEN GOTO @onePlayer:
IF INKEY$ = "2" THEN GOTO @twoPlayer:
IF INKEY$ = "I" THEN GOTO @instructions:
goto @KEY:
@onePlayer:
print at 20,0;"                                                            ";
let players=1
goto @game:
@twoPlayer:
print at 20,0;"                                                            ";
let players=2
goto @game:
@instructions:
cls
#
FAST
print at 0,12;"%u%m%u%l% %g%o%n%u";
print at 2,0;"Game for one or two player.";
print at 4,0;"Player 1 \## Player 2 \.'.";
print at 6,0;"Each player has two pieces.";
print at 7+1,0;"Player play alternately.";
print at 10,0;"At each move, the player moves  one of his two piece to the     adjacent vacant vertex.";
print at 14,0;"The player loses if he can not  move any of his pieces.";
print at 21,0;"press any key to return to menu";
slow
@key1:
IF INKEY$ = "" THEN GOTO @key1:
cls
goto @menu:
#
@game:
#
let turno = INT(RND*2)
let move = -1
DIM x(5)
LET x(1) = 1
LET x(2) = 2
LET x(3) = -1
LET x(4) = 9
LET x(5) = 0
let end = -1
@nextTurno:

gosub @printPieces:
let turno = 1-turno
gosub @checkEnd:
#
IF players = 2 or turno = 0 THEN GOTO @MOVEPIECE:
let c = 0
@MOVEcomputer:
#IF turno = 0 THEN GOTO @nextTurno:
if c = 0 then let ia = INT(RND*2)
if c <> 0 then let ia = 1 - ia
let error = 0
let empty = 0
if ia = 0 then let i = 9
if ia = 0 then gosub @mover9:
if ia = 1 then let i = 0
if ia = 1 then gosub @mover0:
let c = c+1
if error = 1 then goto @MOVEcomputer:
goto @nextTurno:
print at 21,0;"player ";
if turno = 0 then print at 21,7;"one\##";
if turno = 1 AND players = 2 then print at 21,7;"two\.'";
print at 21,12;"select piece to move";
if turno = 1 AND players = 1 then print at 21,0;"            wait                ";
let move = -1 
@key2:
IF INKEY$ = "1" and turno = 0 THEN let move = 1
IF INKEY$ = "2" and turno = 0 THEN let move = 2
IF INKEY$ = "9" and turno = 1 THEN let move = 9
IF INKEY$ = "0" and turno = 1 THEN let move = 0
if move = -1 then GOTO @key2:
let error = 0
let empty = 0
let i = 0
if move = 1 then gosub @mover1:
if move = 2 then gosub @mover2:
if move = 9 then gosub @mover9:
if move = 0 then gosub @mover0:
if error = 1 then goto @MOVEPIECE:
let move = -1
goto @nextTurno:
#
stop
rem todo quitar el stop de arriba
#
#
stop
@printPieces:
if x(1) = -1 then print at 3,7+1;"  "; at 4,7+1;"  "; at 3,6;" ";
if x(2) = -1 then print at 3,22;"  "; at 4,22;"  "; at 3,25;" ";
if x(3) = -1 then print at 10,15;"  "; at 11,15;"  "; at 10,13;" ";
if x(4) = -1 then print at 17,7+1;"  "; at 18,7+1;"  "; at 17,6;" ";
if x(5) = -1 then print at 17,22;"  "; at 18,22;"  "; at 17,25;" ";
#
if x(1) = 1 or x(1) = 2 then print at 3,7+1;"\##\##"; at 4,7+1;"\##\##"; at 3,6;x(1);
if x(2) = 1 or x(2) = 2 then print at 3,22;"\##\##"; at 4,22;"\##\##"; at 3,25;x(2);
if x(3) = 1 or x(3) = 2 then print at 10,15;"\##\##"; at 11,15;"\##\##"; at 10,13;x(3);
if x(4) = 1 or x(4) = 2 then print at 17,7+1;"\##\##"; at 18,7+1;"\##\##"; at 17,6;x(4);
if x(5) = 1 or x(5) = 2 then print at 17,22;"\##\##"; at 18,22;"\##\##"; at 17,25;x(5);

if players = 1 then goto @printComputer:
if x(1) = 9 or x(1) = 0 then print at 3,7+1;"\.'\.'"; at 4,7+1;"\.'\.'"; at 3,6;x(1);
if x(2) = 9 or x(2) = 0 then print at 3,22;"\.'\.'"; at 4,22;"\.'\.'"; at 3,25;x(2);
if x(3) = 9 or x(3) = 0 then print at 10,15;"\.'\.'"; at 11,15;"\.'\.'"; at 10,13;x(3);
if x(4) = 9 OR x(4) = 0 then print at 17,7+1;"\.'\.'"; at 18,7+1;"\.'\.'"; at 17,6;x(4);
if x(5) = 9 OR x(5) = 0 then print at 17,22;"\.'\.'"; at 18,22;"\.'\.'"; at 17,25;x(5);
return

@printComputer:
if x(1) = 9 or x(1) = 0 then print at 3,7+1;"\.'\.'"; at 4,7+1;"\.'\.'"; at 3,6;
if x(2) = 9 or x(2) = 0 then print at 3,22;"\.'\.'"; at 4,22;"\.'\.'"; at 3,25;
if x(3) = 9 or x(3) = 0 then print at 10,15;"\.'\.'"; at 11,15;"\.'\.'"; at 10,13;
if x(4) = 9 OR x(4) = 0 then print at 17,7+1;"\.'\.'"; at 18,7+1;"\.'\.'"; at 17,6;
if x(5) = 9 OR x(5) = 0 then print at 17,22;"\.'\.'"; at 18,22;"\.'\.'"; at 17,25;
return

@mover1:
let piece = 1
gosub @checkPosition:
if error = 1 then return
gosub @setPosition:
let x(i) = -1
return

@mover2:
let piece = 2
gosub @checkPosition:
if error = 1 then return
gosub @setPosition:
let x(i) = -1
return

@mover9:
let piece = 9
gosub @checkPosition:
if error = 1 then return
gosub @setPosition:
let x(i) = -1
return

@mover0:
let piece = 0
gosub @checkPosition:
if error = 1 then return
gosub @setPosition:
let x(i) = -1
return

@checkPosition:
print at 20,0;"                            ";
print at 21,0;"            wait                ";
FOR j=1 TO 5
if x(j) = piece then let i = j
if x(j) = -1 then let empty = j
NEXT j

if i = 1 and (empty = 2 or empty = 5) then let error = 1 
if i = 2 and (empty = 1 or empty = 4) then let error = 1
if i = 3 and empty = 3 then let error = 1
if i = 4 and empty = 2 then let error = 1
if i = 5 and empty = 1 then let error = 1
if error = 1 and (players = 2 or (turno = 0 and players = 1)) then print at 20,0;"This pieces can not be moved";
return

@setPosition:
if empty = 1 and i <> 5 and i <> 1 then let x(1) = piece
if empty = 2 and i <> 4 and i <> 2 then let x(2) = piece
if empty = 3 and i <> 3 then let x(3) = piece
if empty = 4 and i <> 2 and i <> 4 then let x(4) = piece
if empty = 5 and i <> 1 and i <> 5 then let x(5) = piece
return

@checkEnd:
let end = -1
if turno = 0 and (x(1) = 1 or x(1) = 2) and(x(4) = 1 or x(4) = 2) and x(2) = -1 then let end = 1
if turno = 0 and (x(2) = 1 or x(2) = 2) and (x(5) = 1 or x(5) = 2) and x(1) = -1 then let end = 1
if turno = 1 and (x(1) = 9 or x(1) = 0) and (x(4) = 9 or x(4) = 0) and x(2) = -1 then let end = 2
if turno = 1 and (x(2) = 9 or x(2) = 0) and (x(5) = 9 or x(5) = 0) and x(1) = -1 then let end = 2
if end = -1 then return
print at 20,0;"                                ";
print at 21,0;"                                ";
if end = 2 then print at 20,0;"       Player 1 \## win";
if end = 1 and players = 2 then print at 20,0;"       Player 2 \.' win";
if end = 1 and players = 1 then print at 20,0;"       Computer \.' win";
print at 21,3;"Press any key to restart";
@key3:
IF INKEY$ = "" THEN GOTO @key3:
goto @menu:
#
return
