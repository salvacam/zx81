@Init:
cls

print at 2,3; "\  \::\::\::\  \  \::\::\::\  \  \  \::\  \  \  \::\  \  \::\::\::\::\  \::";
print at 3,3; "\::    \::   \:: \:: \:: \:: \:: \::    \::";
print at 4,3; "\::    \::   \:: \::  \::  \:: \::\::\::  \::";
print at 5,3; "\::    \::\::\::\::\:: \::     \:: \::    \::";
print at 6,3; "\  \::\::\:: \::   \:: \::     \:: \::\::\::\:: \::\::\::\::";

print at 9,1; "\::\::\::   \::\::   \::\::\::  \::  \::   \::  \::\::\::";
print at 10,1; "\::  \:: \::  \:: \::     \::  \::\::  \:: \::";
print at 11,1; "\::\::\::  \::  \:: \::     \::  \:: \:: \:: \:: \::\::";
print at 12,1; "\:: \::  \::\::\::\:: \::     \::  \::  \::\:: \::   \::";
print at 13,1; "\::  \:: \::  \::  \::\::\::  \::  \::   \::  \::\::\::";

PRINT AT 17,10; "Z to shoot";
print at 20,5; "Press any key to start";

@Key:
IF INKEY$ = "" then goto @Key:

cls

let count=1
let posC1=1
let posC2=6
let posC3=11
let posC4=16
let distC1=7+1
let distC2=7+1
let distC3=7+1
let distC4=7+1
let movC1=0
let movC2=0
let movC3=0
let movC4=0

let posx = 0
let posy = 20

let change = 1
let shoot = 0
let light = 18

let A$ = "  \: \:'"
let B$ = " \ :\!!\: "
let C$ = " \:' \':"

print at 7,1; "%O";
print at 7,5; "%O";

print at 4,2; "%O";
print at 4,4; "%O";

print at 1,3; "%O";

for C = 0 to 20
print at C,7; "\::";
if c = 7 then print at C,7; "%1";
if c = 4 then print at C,7; "%2";
if c = 1 then print at C,7; "%3";

if c = light then print at C,7; "%:";
print at C,31; "\##";
next C

# Pintar camellos

print at posC1,distC1; A$; at posC1+1,distC1; B$; at posC1+2,distC1; C$;
print at posC2,distC2; A$; at posC2+1,distC2; B$; at posC2+2,distC2; C$;
print at posC3,distC3; A$; at posC3+1,distC3; B$; at posC3+2,distC3; C$;
print at posC4,distC4; A$; at posC4+1,distC4; B$; at posC4+2,distC4; C$;

print at posy,posx; "*"

@Bucle:
	let count = count + 1
	if count = 12 then gosub @MoveEnemies:

	if shoot = 0 then gosub @MoveBall:
	if shoot = 1 then gosub @UpBall:

	if movC1 > 0 then gosub @MoveC1:
	if movC2 > 0 then gosub @MoveC2:
	if movC3 > 0 then gosub @MoveC3:
	if movC4 > 0 then gosub @MoveC4:

	print at light,7; "\::";
	let light = light - 1;
	if light = 12 then let light = 18; 
	print at light,7; "%:";

    IF INKEY$="Z" and shoot = 0 then let shoot = 1
goto @Bucle:

@MoveEnemies:
let count = 1
if movC2 = 0 then let movC2 = INT (RND * 3)
if movC3 = 0 then let movC3 = INT (RND * 3)
if movC4 = 0 then let movC4 = INT (RND * 3)
return

@MoveBall:
    print at posy,posx; " "
    let posx = posx + change
    if posx = 6 then let change = -1
    print at posy,posx; "*"
    if posx = 0 then let change = 1
return

@UpBall:
# mover la pelota
	print at posy,posx; " "
    let posy = posy - 2
    if posx = 1 and posy <= 7 then let shoot = 0
    if posx = 1 and posy <= 7 then let movC1 = 1
    if posx = 1 and posy <= 7 then let posy = 20

    if posx = 5 and posy <= 7 then let shoot = 0
    if posx = 5 and posy <= 7 then let movC1 = 1
    if posx = 5 and posy <= 7 then let posy = 20

    if posx = 2 and posy <= 4 then let shoot = 0
    if posx = 2 and posy <= 4 then let movC1 = 2
    if posx = 2 and posy <= 4 then let posy = 20

    if posx = 4 and posy <= 4 then let shoot = 0
    if posx = 4 and posy <= 4 then let movC1 = 2
    if posx = 4 and posy <= 4 then let posy = 20

    if posx = 3 and posy <= 1 then let shoot = 0
    if posx = 3 and posy <= 1 then let movC1 = 3
    if posx = 3 and posy <= 1 then let posy = 20
    
    if posy = 0 then let shoot = 0
    if posy = 0 then let posy = 20

    print at posy,posx; "*"
return

@End1:
    print at 20,12; "$ You win $";
    @Key1:
    IF INKEY$ = "" then goto @Key1:
    goto @Init:

@End2:
    print at 20,12; "$ You lose $";
    @Key2:
    IF INKEY$ = "" then goto @Key2:
    goto @Init:

@MoveC1:
	let movC1 = movC1 - 1
    print at posC1+2, distC1; "    ";
    let posC1 = posC1 - 1
    print at posC1, distC1; A$; at posC1+1, distC1; B$; at posC1+2, distC1; C$;

    let distC1 = distC1 + 1
    print at posC1, distC1; A$; at posC1+1, distC1; B$; at posC1+2, distC1; C$;

    print at posC1, distC1; "    ";
    let posC1 = posC1 + 1
    print at posC1, distC1; A$; at posC1+1, distC1; B$; at posC1+2, distC1; C$;

    print at posC1, distC1; "    ";
    let posC1 = posC1 + 1
    print at posC1, distC1; A$; at posC1+1, distC1; B$; at posC1+2, distC1; C$;

    let distC1 = distC1 + 1
    print at posC1, distC1; A$; at posC1+1, distC1; B$; at posC1+2, distC1; C$;

    print at posC1+2, distC1; "    ";
    let posC1 = posC1 - 1
    print at posC1, distC1; A$; at posC1+1, distC1; B$; at posC1+2, distC1; C$;
    if distC1 >= 28 then goto @End1:
return

@MoveC2:
	let movC2 = movC2 - 1
    print at posC2+2, distC2; "    ";
    let posC2 = posC2 - 1
    print at posC2, distC2; A$; at posC2+1, distC2; B$; at posC2+2, distC2; C$;

    let distC2 = distC2 + 1
    print at posC2, distC2; A$; at posC2+1, distC2; B$; at posC2+2, distC2; C$;

    print at posC2, distC2; "    ";
    let posC2 = posC2 + 1
    print at posC2, distC2; A$; at posC2+1, distC2; B$; at posC2+2, distC2; C$;

    print at posC2, distC2; "    ";
    let posC2 = posC2 + 1
    print at posC2, distC2; A$; at posC2+1, distC2; B$; at posC2+2, distC2; C$;

    let distC2 = distC2 + 1
    print at posC2, distC2; A$; at posC2+1, distC2; B$; at posC2+2, distC2; C$;

    print at posC2+2, distC2; "    ";
    let posC2 = posC2 - 1
    print at posC2, distC2; A$; at posC2+1, distC2; B$; at posC2+2, distC2; C$;
    if distC2 >= 28 then goto @End2:
return

@MoveC3:
	let movC3 = movC3 - 1
    print at posc3+2, distc3; "    ";
    let posC3 = posC3 - 1
    print at posC3, distC3; A$; at posC3+1, distC3; B$; at posC3+2, distC3; C$;

    let distC3 = distC3 + 1
    print at posC3, distC3; A$; at posC3+1, distC3; B$; at posC3+2, distC3; C$;

    print at posC3, distC3; "    ";
    let posC3 = posC3 + 1
    print at posC3, distC3; A$; at posC3+1, distC3; B$; at posC3+2, distC3; C$;

    print at posC3, distC3; "    ";
    let posC3 = posC3 + 1
    print at posC3, distC3; A$; at posC3+1, distC3; B$; at posC3+2, distC3; C$;

    let distC3 = distC3 + 1
    print at posC3, distC3; A$; at posC3+1, distC3; B$; at posC3+2, distC3; C$;

    print at posC3+2, distC3; "    ";
    let posC3 = posC3 - 1
    print at posC3, distC3; A$; at posC3+1, distC3; B$; at posC3+2, distC3; C$;
    if distC3 >= 28 then goto @End2:
return

@MoveC4:
	let movC4 = movC4 - 1
    print at posc4+2, distc4; "    ";
    let posC4 = posC4 - 1
    print at posC4, distC4; A$; at posC4+1, distC4; B$; at posC4+2, distC4; C$;

    let distC4 = distC4 + 1
    print at posC4, distC4; A$; at posC4+1, distC4; B$; at posC4+2, distC4; C$;

    print at posC4, distC4; "    ";
    let posC4 = posC4 + 1
    print at posC4, distC4; A$; at posC4+1, distC4; B$; at posC4+2, distC4; C$;

    print at posC4, distC4; "    ";
    let posC4 = posC4 + 1
    print at posC4, distC4; A$; at posC4+1, distC4; B$; at posC4+2, distC4; C$;

    let distC4 = distC4 + 1
    print at posC4, distC4; A$; at posC4+1, distC4; B$; at posC4+2, distC4; C$;

    print at posC4+2, distC4; "    ";
    let posC4 = posC4 - 1
    print at posC4, distC4; A$; at posC4+1, distC4; B$; at posC4+2, distC4; C$;
    if distC4 >= 28 then goto @End2:
return
