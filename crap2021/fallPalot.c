#include <stdio.h>
#include <stdlib.h>
#include <zx81.h>
#include <string.h>

void printCenter(int numLiner, char* textCenter)
{	
	int lenCenter = strlen(textCenter);
	int whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner,whiteSpace);
	printf("%s",textCenter);	
}

uchar GetChar(char cY, char cX)
{
    return wpeek(wpeek(16396)+1+cX+cY*33);
}

void SetChar(char cX, char cY, char cValue){
	bpoke(wpeek(16396)+1+cY+cX*33,cValue);
}

void printPalote(int y)
{
	SetChar(8,y,132);
	SetChar(8,y+1,5);
	
	SetChar(9,y,133);
	SetChar(9,y+1,5);

	SetChar(10,y,134);
	SetChar(10,y+1,5);
}

void deletePalote (int y)
{
	SetChar(8,y,0);
	SetChar(8,y+1,0);
	
	SetChar(9,y,0);
	SetChar(9,y+1,0);

	SetChar(10,y,0);
	SetChar(10,y+1,0);
}

int main(void)
{

	int hiScore = 0;	
	int a;

INITGAME:

	zx_asciimode(0);
	for (a = 0; a < 3; a++)
    {
		zx_setcursorpos(a+3,9);
		printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128,128,128,128,128,128,128);
	}
	zx_asciimode(1);
	zx_setcursorpos(4,10);
	printf("FALL");
	zx_setcursorpos(4,16);
	printf("PALO");
	zx_setcursorpos(4,21);
	printf("T");

    printCenter(10, "O left P right");
    printCenter(14, "not hit with shields and walls");
    printCenter(15, "press any key to start");
    
    if (hiScore > 0) {
		printCenter(19,"hiscore    ");
		zx_setcursorpos(19,19);
		printf("%d", hiScore);
	}
    
    printCenter(23, "2021 salvacam");

	fgetc_cons();        // wait for keypress
	zx_cls();


  	zx_asciimode(0);
	int score = 0;
  	for (a = 0; a < 24; a=a+3)
    {

		SetChar(a,0,128);
		SetChar(a,1,133);

		SetChar(a+1,0,4);
		SetChar(a+1,1,131);

		SetChar(a+2,0,1);
		SetChar(a+2,1,3);

		SetChar(a,30,128);
		SetChar(a,31,133);

		SetChar(a+1,30,4);
		SetChar(a+1,31,131);

		SetChar(a+2,30,1);
		SetChar(a+2,31,3);
	}

	int y = 4;

	for (;y <13; y++) {

		SetChar(5,y,0);
		SetChar(5,y+1,133);
		SetChar(5,y+2,134);

		SetChar(6,y,0);
		SetChar(6,y+1,133);
		SetChar(6,y+2,5);

		SetChar(7,y,0);
		SetChar(7,y+1,133);
		SetChar(7,y+2,130);

		in_Wait(15);
	}

	SetChar(5,y,0);
	SetChar(5,y+1,0);
	SetChar(5,y+2,0);

	SetChar(6,y,0);
	SetChar(6,y+1,0);
	SetChar(6,y+2,131);
	SetChar(6,y+3,131);
	SetChar(6,y+4,131);

	SetChar(7,y,0);
	SetChar(7,y+1,0);
	SetChar(7,y+2,7);
	SetChar(7,y+3,7);
	SetChar(7,y+4,6);

	in_Wait(15);

    y = y + 2;

	SetChar(6,y,0);
	SetChar(6,y+1,0);
	SetChar(6,y+2,0);

	SetChar(7,y,0);
	SetChar(7,y+1,0);
	SetChar(7,y+2,0);

	SetChar(8,y,132);
	SetChar(8,y+1,5);
	SetChar(8,y+2,0);

	SetChar(9,y,133);
	SetChar(9,y+1,5);
	SetChar(9,y+2,0);

	SetChar(10,y,134);
	SetChar(10,y+1,5);

	int countWall = 1;
	int countShield = 1;
	int printShield = 9;
	int exit = 0;
	int yShield;
	int timePause = 15;
	
	do
    {    	
    	countShield++;
    	score++;
		in_Wait(timePause);
		deletePalote(y);
		scrolluptxt();

  		if (GetChar(10,y) != 0 || GetChar(10,y+1) != 0 ) {
			exit = 1;
			break;
		}
		printPalote(y);

		if (countWall == 3) {
			countWall = 1;
			SetChar(23,0,1);
			SetChar(23,1,3);

			SetChar(23,30,1);
			SetChar(23,31,3);

		} else if (countWall == 2) {
			countWall = 3;
			SetChar(23,0,4);
			SetChar(23,1,131);

			SetChar(23,30,4);
			SetChar(23,31,131);
		} else if (countWall == 1) {
			countWall = 2;
			SetChar(23,0,128);
			SetChar(23,1,133);

			SetChar(23,30,128);
			SetChar(23,31,133);
		}

		if (score > 52 && score < 103) {
			timePause = 10;
			printShield = 6;

		} else if (score > 102) {
			timePause = 5;
			printShield = 2;
		}

		if (countShield == printShield) {
			yShield = rand() % 26 + 2;
			SetChar(23,yShield,130);
			SetChar(23,yShield+1,128);
			SetChar(23,yShield+2,129);

    		if (score > 500 && yShield < 22) {
				SetChar(23,yShield+6,130);
				SetChar(23,yShield+6+1,128);
				SetChar(23,yShield+6+2,129);
    		}
		}
		if (countShield == printShield + 1) {
			SetChar(23,yShield,132);
			SetChar(23,yShield+1,177);
			SetChar(23,yShield+2,7);
    		if (score > 500 && yShield < 22) {
				SetChar(23,yShield+6,132);
				SetChar(23,yShield+6+1,177);
				SetChar(23,yShield+6+2,7);
    		}
		}
		if (countShield == printShield + 2) {
			countShield = 1;

			SetChar(23,yShield,2);
			SetChar(23,yShield+1,128);
			SetChar(23,yShield+2,1);
    		if (score > 500 && yShield < 22) {

				SetChar(23,yShield+1,2);
				SetChar(23,yShield+1+1,128);
				SetChar(23,yShield+1+2,1);
    		}
		}
		if (countShield > printShield+2) {
			countShield = 1;
		}

		if (in_KeyPressed(in_LookupKey('O'))) {
	  		if (GetChar(7,y-1) != 0 || GetChar(8,y-1) != 0 || GetChar(9,y-1) != 0 ) {
				exit = 1;
				break;
			}
			deletePalote(y);
	        y--;
			printPalote(y);
	    }

	    if (in_KeyPressed(in_LookupKey('P'))) {
	  		if (GetChar(7,y+2) != 0 || GetChar(8,y+2) != 0 || GetChar(9,y+2) != 0 ) {
				exit = 1;
				break;
			}
			deletePalote(y);
	        y++;
			printPalote(y);
	    }



	} while (exit == 0);


	zx_cls();

  	zx_asciimode(1);
	
	printCenter(10,"your score    ");
	zx_setcursorpos(10,21);
	printf("%d", score);
	
	in_Wait(50);
	printCenter(12,"hiscore  ");
	zx_setcursorpos(12,20);
	printf("%d", hiScore);

	if (score > hiScore ) {
		hiScore = score;	
		in_Wait(20);
		printCenter(12,"NEW HISCORE    ");
		zx_setcursorpos(12,21);
		printf("%d", hiScore);
	}

	in_Wait(50);
	printCenter(15, "press any key to restart");

  	zx_asciimode(0);

	fgetc_cons();        // wait for keypress
	zx_cls();

	goto INITGAME;

	return 0;
}