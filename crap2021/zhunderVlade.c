#include <stdio.h>
#include <stdlib.h>
#ifndef zx80
#include <zx81.h>
#endif
#include <string.h>

int main(void)
{

  #ifdef zx80
  gen_tv_field_init(0);
  #endif

  zx_setcursorpos(10,7);
  printf("push M to upload");

  zx_setcursorpos(12,4);
  printf("push any key to begin");

  fgetc_cons();        // wait for keypress

  int puntMax = 500;

  reset:
  zx_cls();
  int ax=30;
  int bx=30;
  int cx=30;
  int dx=30;
  int b1=19;
  int b2=19;
  int b3=19;
  int c1=19;
  int c2=19;
  int c3=19;
  int d1=19;
  int d2=19;
  int d3=19;
  
  int key;
  int i;
  for (i = 0; i < 31; ++i)
  {
    zx_setcursorpos(0,i);
    zx_asciimode(0);
    #ifdef zx80
    printf("%c", 138);
    #else
    printf("%c", 137);
    #endif
    zx_setcursorpos(18,i);
    #ifdef zx80
    printf("%c", 139);
    #else
    printf("%c", 138);
    #endif
    zx_asciimode(1);
  }

  zx_setcursorpos(20,9);
  printf("score");

  zx_setcursorpos(21,17);
  printf("%d", puntMax);

  zx_setcursorpos(21,9);
  printf("hiscore");

  int jy=9;
  int punt=0;

  int a1=(rand()%17)+1;
  int a2=(rand()%17)+1;
  int a3=(rand()%17)+1;

  init:

  if (jy<16) {
    zx_setcursorpos(jy+2,3);
    printf("   ");
  }

  if (jy>1) {
    zx_setcursorpos(jy-1,3);
    printf("   ");
  }

  zx_setcursorpos(jy,3);
  zx_asciimode(0);
  #ifdef zx80
  printf("%c%c%c", 133, 3, 0);
  #else
  printf("%c%c%c", 130, 131, 0);
  #endif
  zx_setcursorpos(jy+1,3);
  #ifdef zx80
  printf("%c%c%c", 134, 191, 133);
  #else
  printf("%c%c%c", 132, 191, 130);
  #endif
  zx_asciimode(1);

  if (ax > 0) {
    zx_setcursorpos(a1,ax);
    printf("* ");
    zx_setcursorpos(a2,ax);
    printf("* ");
    zx_setcursorpos(a3,ax);
    printf("* ");
  } 

  if (ax <= 0) {
    zx_setcursorpos(a1,ax);
    printf("   ");
    zx_setcursorpos(a2,ax);
    printf("   ");
    zx_setcursorpos(a3,ax);
    printf("   ");
    ax=30;
    a1=(rand()%17)+1;
    a2=(rand()%17)+1;
    a3=(rand()%17)+1;
  }

  if (punt == 108) {
    bx = 30;    
    b1=(rand()%17)+1;
    b2=(rand()%17)+1;
    b3=(rand()%17)+1;
  }
  
  if (bx > 0 && punt >= 108) {
    zx_setcursorpos(b1,bx);
    printf("* ");
    zx_setcursorpos(b2,bx);
    printf("* ");
    zx_setcursorpos(b3,bx);
    printf("* ");
  } 

  if (bx <= 0) {    
    zx_setcursorpos(b1,bx);
    printf("   ");
    zx_setcursorpos(b2,bx);
    printf("   ");
    zx_setcursorpos(b3,bx);
    printf("   ");
    bx=30;
    b1=(rand()%17)+1;
    b2=(rand()%17)+1;
    b3=(rand()%17)+1;
  }

  if (punt == 249) {
    cx = 30;    
    c1=(rand()%17)+1;
    c2=(rand()%17)+1;
    c3=(rand()%17)+1;
  }
  
  if (cx > 0 && punt >= 249) {
    zx_setcursorpos(c1,cx);
    printf("* ");
    zx_setcursorpos(c2,cx);
    printf("* ");
    zx_setcursorpos(c3,cx);
    printf("* ");
  } 

  if (cx <= 0) {    
    zx_setcursorpos(c1,cx);
    printf("   ");
    zx_setcursorpos(c2,cx);
    printf("   ");
    zx_setcursorpos(c3,cx);
    printf("   ");
    cx=30;
    c1=(rand()%17)+1;
    c2=(rand()%17)+1;
    c3=(rand()%17)+1;
  }

  if (punt == 324) {
    dx = 30;    
    d1=(rand()%17)+1;
    d2=(rand()%17)+1;
    d3=(rand()%17)+1;
  }
  
  if (dx > 0 && punt >= 324) {
    zx_setcursorpos(d1,dx);
    printf("* ");
    zx_setcursorpos(d2,dx);
    printf("* ");
    zx_setcursorpos(d3,dx);
    printf("* ");
  } 

  if (dx <= 0) {    
    zx_setcursorpos(d1,dx);
    printf("   ");
    zx_setcursorpos(d2,dx);
    printf("   ");
    zx_setcursorpos(d3,dx);
    printf("   ");
    dx=30;
    d1=(rand()%17)+1;
    d2=(rand()%17)+1;
    d3=(rand()%17)+1;
  }

  zx_setcursorpos(20,17);
  printf("%d",punt);

  key = getk();
  if (key == 109) {
    jy--;
  }
  else {
    jy++;
  }
  
  #ifdef zx80
  gen_tv_field();
  #endif

  if ( (ax == 3 || ax == 4 || ax == 5) && 
    ( (jy==a1 || jy+1==a1) || (jy==a2 || jy+1==a2) || (jy==a3 || jy+1==a3) ) ) {
    goto end;
  }

  if ( punt>108 && (bx == 3 || bx == 4 || bx == 5) && 
    ( (jy==b1 || jy+1==b1) || (jy==b2 || jy+1==b2) || (jy==b3 || jy+1==b3) ) ) {
    goto end;
  }

  if ( punt>256 && (cx == 3 || cx == 4 || cx == 5) && 
    ( (jy==c1 || jy+1==c1) || (jy==c2 || jy+1==c2) || (jy==c3 || jy+1==c3) ) ) {
    goto end;
  }

  if ( punt>302 && (dx == 3 || dx == 4 || dx == 5) && 
    ( (jy==d1 || jy+1==d1) || (jy==d2 || jy+1==d2) || (jy==d3 || jy+1==d3) ) ) {
    goto end;
  }

  punt=punt+1;
  ax--;
  bx--;
  cx--;
  dx--;

  if (jy == 0 || jy == 17) 
  {
    goto end;
  }

  goto init;


  end:

  if (jy<16) {
    zx_setcursorpos(jy+2,3);
    printf("   ");
  }

  if (jy>1) {
    zx_setcursorpos(jy-1,3);
    printf("   ");
  }

  zx_setcursorpos(jy,3);
  zx_asciimode(0);
  printf("%c%c%c", 6, 138, 134);
  zx_setcursorpos(jy+1,3);
  printf("%c%c%c", 134, 137, 6);
  zx_asciimode(1);

  zx_setcursorpos(10,11);
  printf("game over");

  fgetc_cons();        // wait for keypress

  if (punt > puntMax)
  {
    puntMax=punt;
  }

  zx_setcursorpos(12,4);
  printf("push any key to restart");
  
  #ifdef zx80
  gen_tv_field();
  #endif

  fgetc_cons();        // wait for keypress

  zx_cls();
  goto reset;

}