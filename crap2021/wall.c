#include <stdio.h>
#include <stdlib.h>
#include <zx81.h>

void printTitle (void) {
	int a;
	zx_asciimode(0);
	for (a = 0; a < 3; a++)
    {
		zx_setcursorpos(a+3,9);
		printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 
			128,128,128,128,128,128,128,128,128,128,128,128,128,128,128);
	}
	zx_asciimode(1);
	zx_setcursorpos(4,10);
	printf("WALL");
	zx_setcursorpos(4,15);
	printf("OF");
	zx_setcursorpos(4,18);
	printf("CHINA");
}

void printCenter(int numLiner, char* textCenter)
{	
	int lenCenter = strlen(textCenter);
	int whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner,whiteSpace);
	printf("%s",textCenter);	
}

void printCharacter (int x, int y)
{
    bpoke(wpeek(16396)+1+y+(x*33),139);
}

void deleteCharacter (int x, int y)
{
    bpoke(wpeek(16396)+1+y+(x*33),0);
}

void printEne (int x, int y, int number)
{
	zx_asciimode(0);
	int c = 0;
	for (c = 0; c < number; c++) {
    	bpoke(wpeek(16396)+1+y+((x+c)*33),142);
	}
	zx_asciimode(1);
}

void deleteEne (char x, char y, char number)
{
	int c = 0;
	for (c = 0; c < number; c++) {
    	bpoke(wpeek(16396)+1+y+((x+c)*33),0);
	}
}

uchar GetChar(char cX, char cY)
{
    return wpeek(wpeek(16396)+1+cX+cY*33);
}


int main(void)
{
	int ene;
	int cicle, moveH, moveV;
		
	int x, y, xEne, yEne, dirEne, a;

TITLE:
	ene = 1;
	zx_cls(); 
	
	printTitle();	
	printCenter(16, "move \" to m escape from :");
	
	zx_asciimode(0);
    zx_setcursorpos(16,8);
    printf("%c", 139);
    zx_setcursorpos(16,13);
    printf("%c", 136);
    zx_setcursorpos(16,27);
    printf("%c", 142);
	zx_asciimode(1);

	printCenter(18, "press any key to start");
	printCenter(21, "2021 salvacam");

	char marquesina [37] = "Q up, A down, O left, P right, ";
	char marquesinaCopy = marquesina;

    zx_setcursorpos(10,3);
    printf("keys: ");

	do {
    	zx_setcursorpos(10,9);
    	printf("%.20s",marquesinaCopy);

		strcat(marquesinaCopy, " ");
		strlcpy(marquesinaCopy, marquesinaCopy + 1, strlen(marquesinaCopy) -1);

		if (strlen(marquesinaCopy) < 20) {
			strcat(marquesinaCopy, "Q up, A down, O left, P right, ");
		}

    	in_Pause(10);
	}
	while (getk() == '\0');

INIT:
    
    x = 10;
    y = 0;
	xEne = (22 - ene) /2;
	yEne = 29;
    dirEne = 0;
	cicle = 0;
    moveH = 0;
    moveV = 0;

	zx_cls();
	
	for (a = 0; a<24; a++){
		zx_asciimode(0);
	    zx_setcursorpos(a,31);
	    printf("%c", 136);
		zx_asciimode(1);
	}
	
	printCharacter(x,y);

	printEne(xEne, yEne, ene);
	
MARK:

    cicle = cicle + 1;
    if (cicle >= 350) {
    	moveH = 0;
    	moveV = 0;
    	cicle = 1;
    }

	if (moveV == 1 && cicle % 5 == 0 ) {
		moveV = 0;
	}

	if (moveH == 1 && cicle % 5 == 0 ) {
		moveH = 0;
	}

	if (cicle % 10 == 0 ) {
		deleteEne(xEne, yEne, ene);
		if (yEne > 0 && dirEne == 0 && yEne > y) {			
			yEne--;
		} else {	
			dirEne = 1;
			yEne++;
		}
		if (yEne == 30 || yEne < y){
			dirEne = 0;
		}
		if (x < xEne){
			xEne--;
		}

		if (x >= xEne+ene){
			xEne++;
		}
     
 	   	for (a = 0; a <= ene; a++) {
    		//bpoke(wpeek(16396)+1+y+((x+c)*33),142);
        	if (GetChar(yEne,xEne+a) == 139 ) goto LOSE; 
		}

		printEne(xEne, yEne, ene);
	}

    if (moveH == 0 && in_KeyPressed(in_LookupKey('O')) && y > 0) {
    	moveH = 1;
		deleteCharacter(x,y);
        y--;
        if (GetChar(y,x) == 136 ) goto WIN;
        if (GetChar(y,x) == 142 ) goto LOSE;
		printCharacter(x,y);
    }

    if (moveH == 0 && in_KeyPressed(in_LookupKey('P')) && y < 31) {
    	moveH = 1;
        deleteCharacter(x,y);
        y++;
        if (GetChar(y,x) == 136 ) goto WIN;
        if (GetChar(y,x) == 142 ) goto LOSE;
		printCharacter(x,y);
    }

    if (moveV == 0 && in_KeyPressed(in_LookupKey('Q')) && x > 0) {
    	moveV = 1;
        deleteCharacter(x,y);
        x--;        
        if (GetChar(y,x) == 136 ) goto WIN;
        if (GetChar(y,x) == 142 ) goto LOSE;
		printCharacter(x,y);
    }

    if (moveV == 0 && in_KeyPressed(in_LookupKey('A')) && x < 23) {
    	moveV = 1;
        deleteCharacter(x,y);
        x++;
        if (GetChar(y,x) == 136 ) goto WIN;
        if (GetChar(y,x) == 142 ) goto LOSE;
        printCharacter(x,y);
    }


goto MARK;


WIN:
    zx_setcursorpos(12,12);
    printCenter(12,"YOU WIN");
    ene++;

	fgetc_cons();        // wait for keypress

goto INIT;

LOSE:
	deleteEne(xEne, yEne, ene);
	deleteCharacter(x,y);
    printCenter(12,"YOU LOSE");
    printCenter(15,"you have escaped    times");

    zx_setcursorpos(15,21);
    if (ene > 9){
    	zx_setcursorpos(15,20);
    }
    printf("%d", ene);

	fgetc_cons();        // wait for keypress

goto TITLE;

	return 0;
}