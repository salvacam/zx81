#include <stdio.h>
#include <stdlib.h>
#include <zx81.h>
#include <string.h>
#include "snailMaze.h"

uchar GetChar(char cX, char cY)
{
    return wpeek(wpeek(16396)+1+cX+cY*33);
}

void printTitle (void) {
	int a;
	zx_asciimode(0);
	for (a = 0; a < 3; a++)
    {
		zx_setcursorpos(a+3,10);
		printf("%c%c%c%c%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128,128,128,128,128);
	}
	zx_asciimode(1);
	zx_setcursorpos(4,11);
	printf("SNAIL");
	zx_setcursorpos(4,17);
	printf("MAZE");
}

void printMaze (int screen[19][30], int goal[2], int stage, int time)
{
    int b;
    int a;

	zx_asciimode(0);

    for (a = 2; a < 23; a=a+20)
    {
		zx_setcursorpos(a,0);
		printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128);
	}

	zx_setcursorpos(3,0);
    for (a = 0; a < 19; ++a)
    {
		printf("%c", 128);
        for (b = 0; b < 30; ++b)
        {
    		if (screen[a][b] == 1) {
    			printf("%c", 128);
    		}
        	else {
    			printf("%c", 0);
        	}
		}
		printf("%c", 128);
	}
	zx_asciimode(1);
	zx_setcursorpos(goal[0],goal[1]);
	printf("g");

	zx_setcursorpos(0,2);
	printf("stage: %d", stage);
	
	zx_setcursorpos(0,21);
	printf("time: %d ", time);

    return 0;
}

void printTime (int time)
{
	zx_setcursorpos(0,27);
	printf("%d ", time);

	return 0;
}


int checkFinish (int x, int y, int goal[3])
{
	if (x == goal[0] && y == goal[1]) {
		return 1;
	}
	return 0;
}

void printCharacter (int x, int y)
{
    zx_setcursorpos(x,y);
    printf("*");
}

void deleteCharacter (int x, int y)
{
    zx_setcursorpos(x,y);
    printf(" ");
}

void printCenter(int numLiner, char* textCenter)
{	
	int lenCenter = strlen(textCenter);
	int whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner,whiteSpace);
	printf("%s",textCenter);	
}

void printPalote(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y+1);
	printf("%c%c%c", 6, 3, 4);
	zx_setcursorpos(x+1,y+1);
	printf("%c%c%c", 5,133,130);
	zx_setcursorpos(x+2,y+1);
	printf("%c%c%c", 134,131,1);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c%c", 2, 132,128,3);
	zx_setcursorpos(x+4,y+1);
	printf("%c%c%c", 133,128,0);
	zx_setcursorpos(x+5,y+1);
	printf("%c%c%c", 133,128,0);
	zx_setcursorpos(x+6,y+1);
	printf("%c%c%c", 133,128,130);
    zx_asciimode(1);
}

void printString (int x, int y, char* text)
{
    zx_setcursorpos(x,y);
    printf(text);
}
void printMicroPaloteHead(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 133, 134);
    zx_asciimode(1);
}

void printMicroPalote1(int x, int y){
	printMicroPaloteHead(x, y);
    zx_asciimode(0);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 133, 5);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 133, 130);
    zx_asciimode(1);
}

void printMicroPalote2(int x, int y){
	printMicroPaloteHead(x, y);
    zx_asciimode(0);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 133, 130);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 7, 130);
    zx_asciimode(1);
}

void printMicroPalote3(int x, int y){
	printMicroPaloteHead(x, y);
    zx_asciimode(0);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 133, 5);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 7, 3);
    zx_asciimode(1);
}

void printMicroPalote4(int x, int y){
	printMicroPaloteHead(x, y);
    zx_asciimode(0);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 132, 5);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 7, 130);
    zx_asciimode(1);
}

void printMicroPaloteHeadL(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 6, 5, 0);
    zx_asciimode(1);
}

void printMicroPaloteL1(int x, int y){
	printMicroPaloteHeadL(x, y);
    zx_asciimode(0);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 133, 5, 0);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 129, 5, 0);
    zx_asciimode(1);
}

void printMicroPaloteL2(int x, int y){
	printMicroPaloteHeadL(x, y);
    zx_asciimode(0);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 129, 5, 0);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 129, 132, 0);
    zx_asciimode(1);
}

void printMicroPaloteL3(int x, int y){
	printMicroPaloteHeadL(x, y);
    zx_asciimode(0);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 133, 5, 0);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 3, 132, 0);
    zx_asciimode(1);
}

void printMicroPaloteL4(int x, int y){
	printMicroPaloteHeadL(x, y);
    zx_asciimode(0);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 133, 7, 0);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 129, 132, 0);
    zx_asciimode(1);
}

int main(void)
{

	int cicle = 0;
    int moveH = 0;
    int moveV = 0;
	
	INIT:
	zx_cls(); 
	
	printTitle();
	printCenter(15, "move * to Goal");
	printCenter(17, "press any key to start");
//	printCenter(21, "2021 salvacam");

	char marquesina [37] = "Q up, A down, O left, P right, ";
	char marquesinaCopy = marquesina;

    zx_setcursorpos(10,3);
    printf("keys: ");

	do {
    	zx_setcursorpos(10,9);
    	printf("%.20s",marquesinaCopy);

		strcat(marquesinaCopy, " ");
		strlcpy(marquesinaCopy, marquesinaCopy + 1, strlen(marquesinaCopy) -1);

		if (strlen(marquesinaCopy) < 20) {
			strcat(marquesinaCopy, "Q up, A down, O left, P right, ");
		}

    	in_Pause(10);
	}
	while (getk() == '\0');


	zx_cls();

	int screen = 1;

	int *goal = goal1;
	int time = time1;	
	printMaze (screen1, goal, screen, time);

	int x = start1[0];
    int y = start1[1];
	printCharacter(x,y);
 
MARK:
    cicle = cicle + 1;
    if (cicle >= 350) {
    	moveH = 0;
    	moveV = 0;
    	cicle = 1;
		time--;
		printTime(time);
    }

	if (time <= 0)
	{
		zx_setcursorpos(1,12);
    	printf("time out");
		fgetc_cons();        // wait for keypress
 		goto INIT;
	}

	if (moveV == 1 && cicle % 20 == 0 ) {
		moveV = 0;
	}

	if (moveH == 1 && cicle % 20 == 0 ) {
		moveH = 0;
	}

    if (moveH == 0 && in_KeyPressed(in_LookupKey('O')) && GetChar(y-1,x) != 128) {
    	moveH = 1;
		deleteCharacter(x,y);
        y--;
		printCharacter(x,y);
    }

    if (moveH == 0 && in_KeyPressed(in_LookupKey('P')) && GetChar(y+1,x) != 128) {
    	moveH = 1;
        deleteCharacter(x,y);
        y++;
		printCharacter(x,y);
    }

    if (moveV == 0 && in_KeyPressed(in_LookupKey('Q')) && GetChar(y,x-1) != 128) {
    	moveV = 1;
        deleteCharacter(x,y);
        x--;        
		printCharacter(x,y);
    }

    if (moveV == 0 && in_KeyPressed(in_LookupKey('A')) && GetChar(y,x+1) != 128) {
    	moveV = 1;
        deleteCharacter(x,y);
        x++;
        printCharacter(x,y);
    }

//TODO remove
 	if (in_KeyPressed(in_LookupKey('M'))) {

		//Last		
    	zx_setcursorpos(1,9);
    	char* marquesinaEnd = "congratulations";

		int lenM = strlen(marquesinaEnd) + 1;
		int z;
		for (z = 0; z < lenM; ++z)
		{
			printf("%c",marquesinaEnd[z]);
		    in_Pause(50);
		}

		fgetc_cons();        // wait for keypress

		zx_cls();
 		goto FINISH;
 	}

//TODO remove
 	if (checkFinish(x,y,goal) == 1 || in_KeyPressed(in_LookupKey('J')) ) {
		if (screen == 4) {
 			zx_cls();
 			screen = 5;
			time = time5;

			goal = goal5;
			printMaze (screen5, goal, screen, time);
			x = start5[0];
	    	y = start5[1];
			printCharacter(x,y);
 		} else if (screen == 3) {
 			zx_cls();
 			screen = 4;
			time = time4;

			goal = goal4;
			printMaze (screen4, goal, screen, time);
			x = start4[0];
	    	y = start4[1];
			printCharacter(x,y);
 		} else if (screen == 2) {
 			zx_cls();
 			screen = 3;
			time = time3;

			goal = goal3;
			printMaze (screen3, goal, screen, time);
			x = start3[0];
	    	y = start3[1];
			printCharacter(x,y);
 		}
 		else if (screen == 1) {
 			zx_cls();
 			screen = 2;
			time = time2;

			goal = goal2;
			printMaze (screen2, goal, screen, time);
			x = start2[0];
	    	y = start2[1];
			printCharacter(x,y);
 		}
		else 
		{
			//Last		
	    	zx_setcursorpos(1,9);
	    	char* marquesinaEnd = "congratulations";

			int lenM = strlen(marquesinaEnd) + 1;
			int z;
			for (z = 0; z < lenM; ++z)
			{
				printf("%c",marquesinaEnd[z]);
			    in_Pause(50);
			}

			fgetc_cons();        // wait for keypress

			zx_cls();
	 		goto FINISH;
 		}
 	}

    goto MARK;


FINISH:

	zx_cls();

	int a;

	zx_asciimode(0);
	for (a = 0; a < 3; a++)
    {
		zx_setcursorpos(a,5);
		printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128);
	}
	zx_asciimode(1);

    printString(1,6,"SEE");
    printString(1,10,"YOU");
    printString(1,14,"IN");
    printString(1,17,"MINI");
    printString(1,22,"PALOT");

	int xPalote = 17;
	a = 3;

    printPalote(xPalote,a);

    zx_asciimode(0);
	
	zx_setcursorpos(4,15);
	printf("%c%c%c%c%c%c%c%c%c%c", 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3);
	zx_setcursorpos(4,14);
	printf("%c", 7);
	zx_setcursorpos(4,25);
	printf("%c", 132);

	for (a = 0; a < 10; a++)
    {
		zx_setcursorpos(5+a,14);
		printf("%c", 5);
		zx_setcursorpos(5+a,25);
		printf("%c", 133);
    }
	zx_setcursorpos(14,15);
	printf("%c%c%c%c%c%c%c%c%c%c", 131,131,131,131,131,131,131,131,131,131,131,131);
	zx_setcursorpos(14,14);
	printf("%c", 130);
	zx_setcursorpos(14,25);
	printf("%c", 129);

	zx_setcursorpos(16,14);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c", 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3);

	zx_setcursorpos(16,13);
	printf("%c", 6);

	zx_setcursorpos(16,13);
	printf("%c", 6);
	zx_setcursorpos(16,26);
	printf("%c", 132);
	for (a = 1; a < 6; a++)
    {
		zx_setcursorpos(16+a,13-a);
		printf("%c", 6);
		zx_setcursorpos(16+a,27-a);
		printf("%c", 6);
    }

	zx_setcursorpos(22,7);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 129,131,131,131,131,131,131,131,131,131,131,131,131,131,129);
	zx_setcursorpos(18,11);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 6, 9,9,9,9,9,9,9,9,9,9,9,9,9,6);


	for (a = 0; a < 3; a++)
    {
		zx_setcursorpos(19+a,12-a);
		printf("%c%c%c%c%c%c%c%c%c%c%c", 9,0,9,0,9,0,9,0,9,0,9);
	}
    zx_asciimode(1);

	zx_setcursorpos(17,14);
	printf("zx81");

	int pausePalote = 50;
    a=15;
	int statusPalot=1;
	int xMiniPalot=10;
	int r;
	do {

		for (r = 0; r < 2; r++)
    	{
			printMicroPalote1(xMiniPalot,a);
			in_Pause(pausePalote);
			a++;
			printMicroPalote2(xMiniPalot,a);
			in_Pause(pausePalote);
			a++;
			if (getk() != '\0') break;
			printMicroPalote3(xMiniPalot,a);
			in_Pause(pausePalote);
			a++;
			printMicroPalote4(xMiniPalot,a);
			in_Pause(pausePalote);
			a++;
		}
		if (getk() != '\0') break;

		for (r = 0; r < 2; r++)
    	{
			a--;
			printMicroPaloteL1(xMiniPalot,a);
			in_Pause(pausePalote);
			a--;
			printMicroPaloteL2(xMiniPalot,a);
			in_Pause(pausePalote);
			a--;
			if (getk() != '\0') break;
			printMicroPaloteL3(xMiniPalot,a);
			in_Pause(pausePalote);
			a--;
			printMicroPaloteL4(xMiniPalot,a);
			in_Pause(pausePalote);
		}
	} while (getk() == '\0');

	goto INIT;

	return 0;
}

