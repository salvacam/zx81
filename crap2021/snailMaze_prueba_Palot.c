#include <stdio.h>
#include <stdlib.h>
#include <zx81.h>
#include <string.h>

uchar GetChar(char cX, char cY)
{
    return wpeek(wpeek(16396)+1+cX+cY*33);
}

void printMaze (int screen[21][32], int goal[2], int stage, int time)
{
    int b;
    int a;

	zx_setcursorpos(2,0);
    zx_asciimode(0);

    for (a = 0; a < 21; ++a) //21-3
    {
        for (b = 0; b < 32; ++b)
        {
    		if (screen[a][b] == 1) {
    			printf("%c", 128);
    		}
        	else {
    			printf("%c", 0);
        	}
		}
	}
	zx_asciimode(1);
	zx_setcursorpos(goal[0],goal[1]);
	printf("g");
/*
	zx_setcursorpos(20,0);
	printf("%d",goal[0]);
	
	zx_setcursorpos(21,0);
	printf("%d",goal[1]);
*/
	zx_setcursorpos(0,2);
	printf("stage: %d", stage);
	
	zx_setcursorpos(0,21);
	printf("time: %d ", time);

    return 0;
}

void printTime (int time)
{
	zx_setcursorpos(0,27);
	printf("%d ", time);

	return 0;
}


int checkFinish (int x, int y, int goal[3])
{
	if (x == goal[0] && y == goal[1]) {
		return 1;
	}
	return 0;
}

void printCharacter (int x, int y)
{
    zx_setcursorpos(x,y);
    printf("*");
}

void deleteCharacter (int x, int y)
{
    zx_setcursorpos(x,y);
    printf(" ");
}

void printMicroPalote1(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 129, 134);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 132, 7);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 7, 130);
    zx_asciimode(1);
}

void printMicroPalote2(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 129, 134);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 132, 7);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 133, 130);
    zx_asciimode(1);
}

void printMicroPalote3(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 129, 134);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 132, 7);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 7, 3);
    zx_asciimode(1);
}

void deleteMicroPalote(int x, int y){
	zx_setcursorpos(x,y);
	printf("   ");
	zx_setcursorpos(x+1,y);
	printf("   ");
	zx_setcursorpos(x+2,y);
	printf("   ");
}


void printPicoPalote1(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 0, 139); //128); //139);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 0, 5);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 0, 130);
    zx_asciimode(1);
}

void printPicoPalote2(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 0, 139);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 0, 130);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 0, 130);
    zx_asciimode(1);
}

void printPicoPalote3(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 0, 139);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 135, 5);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 0, 130);
    zx_asciimode(1);
}

void printPicoPalote4(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 0, 139);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 0, 130);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 133, 1);
    zx_asciimode(1);
}

void deletePicoPalote(int x, int y){
	zx_setcursorpos(x,y);
	printf("  ");
	zx_setcursorpos(x+1,y);
	printf("  ");
	zx_setcursorpos(x+2,y);
	printf("  ");
}

void printMiniPalote1(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0, 129, 134);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0, 132,7);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0, 133,7);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c", 0, 7,130);
    zx_asciimode(1);
}

void printMiniPalote2(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0,129, 134);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0,132,7);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0,133,5);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c", 0,133,130);
    zx_asciimode(1);
}

void printMiniPalote3(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0,129, 134);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0,132,7);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0,132,5);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c", 0,7,130);
    zx_asciimode(1);
}

void printMiniPalote4(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c", 0,129, 134);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c", 0,132,7);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c", 0,132,7);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c", 0,7,3);
    zx_asciimode(1);
}

void deleteMiniPalote(int x, int y){
	zx_setcursorpos(x,y);
	printf("  ");
	zx_setcursorpos(x+1,y);
	printf("  ");
	zx_setcursorpos(x+2,y);
	printf("  ");
	zx_setcursorpos(x+3,y);
	printf("  ");
}

void printPalote1(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c%c%c", 0, 0, 6, 3, 4);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c%c%c", 0, 0, 5,133,130);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c%c%c", 0, 0, 134,131,1);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c%c%c", 0, 0, 133,128,3);
	zx_setcursorpos(x+4,y);
	printf("%c%c%c%c%c", 0, 0, 133,128,0);
	zx_setcursorpos(x+5,y);
	printf("%c%c%c%c%c", 0, 135, 129,128,0);
	zx_setcursorpos(x+6,y);
	printf("%c%c%c%c%c", 0, 2, 0,133,130);
    zx_asciimode(1);
}

void printPalote2(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c%c%c", 0, 0, 6, 3, 4);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c%c%c", 0, 0, 5,133,130);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c%c%c", 0, 0, 134,131,1);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c%c%c", 0, 0, 133,128,0);
	zx_setcursorpos(x+4,y);
	printf("%c%c%c%c%c", 0, 0, 133,128,0);
	zx_setcursorpos(x+5,y);
	printf("%c%c%c%c%c", 0, 0, 133,128,0);
	zx_setcursorpos(x+6,y);
	printf("%c%c%c%c%c", 0, 0, 133,128,130);
    zx_asciimode(1);
}

void printPalote3(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c%c%c", 0, 0, 6, 3, 4);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c%c%c", 0, 0, 5,133,130);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c%c%c", 0, 0, 134,131,1);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c%c%c", 0, 133, 132,128,0);
	zx_setcursorpos(x+4,y);
	printf("%c%c%c%c%c", 0, 0, 133,128,0);
	zx_setcursorpos(x+5,y);
	printf("%c%c%c%c%c", 0, 135, 129,128,0);
	zx_setcursorpos(x+6,y);
	printf("%c%c%c%c%c", 0, 2, 0,133,130);
    zx_asciimode(1);
}

void printPalote4(int x, int y){
    zx_asciimode(0);
	zx_setcursorpos(x,y);
	printf("%c%c%c%c%c", 0, 0, 6, 3, 4);
	zx_setcursorpos(x+1,y);
	printf("%c%c%c%c%c", 0, 0, 5,133,130);
	zx_setcursorpos(x+2,y);
	printf("%c%c%c%c%c", 0, 0, 134,131,1);
	zx_setcursorpos(x+3,y);
	printf("%c%c%c%c%c", 0, 2, 132,128,132);
	zx_setcursorpos(x+4,y);
	printf("%c%c%c%c%c", 0, 0, 133,128,0);
	zx_setcursorpos(x+5,y);
	printf("%c%c%c%c%c", 0, 135, 129,128,131);
	zx_setcursorpos(x+6,y);
	printf("%c%c%c%c%c", 0, 2, 0,0,0);
    zx_asciimode(1);
}

void deletePalote(int x, int y){
	zx_setcursorpos(x,y);
	printf("    ");
	zx_setcursorpos(x+1,y);
	printf("    ");
	zx_setcursorpos(x+2,y);
	printf("    ");
	zx_setcursorpos(x+3,y);
	printf("    ");
	zx_setcursorpos(x+4,y);
	printf("    ");
	zx_setcursorpos(x+5,y);
	printf("    ");
	zx_setcursorpos(x+6,y);
	printf("    ");
}

int main(void)
{

    int a;

	zx_setcursorpos(0,0);
/*
    for (a = 0; a < 21; ++a) //21-3
    {
    	printf("01234567890123456789012345678912");
	}
*/

	int cicle = 0;    
	int key;
    int move = 0;

	// TODO remove in release
    int moveTime = 0;

	INIT:
	zx_cls();
	zx_setcursorpos(4,11);
    printf("snail maze");

	zx_setcursorpos(17,5);
    printf("press any key to start");

	char marquesina [37] = "Q up, A down, O left, P right, ";
	char marquesinaCopy = marquesina;

    zx_setcursorpos(10,3);
    printf("keys: ");

	key = getk();
	while (key == '\0') {
    	zx_setcursorpos(10,9);
    	printf("%.20s",marquesinaCopy);

		strcat(marquesinaCopy, " ");
		strlcpy(marquesinaCopy, marquesinaCopy + 1, strlen(marquesinaCopy) -1);

		if (strlen(marquesinaCopy) < 20) {
			strcat(marquesinaCopy, marquesina);
		}

    	in_Pause(10);
		key = getk();
	}


	zx_cls();

	//goto FINISH;

	int screen = 1;
	int screen1[21][32] = {
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
		{1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
		{1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,0,1,1,0,1,1,1,1,1,1,0,0,1},
		{1,0,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,0,0,0,1,0,1,1},
		{1,0,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,0,0,0,1,0,0,1},
		{1,0,0,1,0,1,0,0,0,1,0,0,0,1,0,1,1,1,1,0,1,1,1,1,1,1,1,0,0,1,0,1},
		{1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1,1,1},
		{1,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,1,0,1},
		{1,1,0,1,1,1,1,0,1,0,1,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,1},
		{1,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,1,0,1,1,1,1,1,1,1,1,0,1,0,1,0,1},
		{1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
		{1,0,1,0,1,0,1,0,0,1,0,0,0,1,0,0,1,0,1,1,1,1,0,0,0,1,1,0,1,1,0,1},
		{1,0,1,0,1,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,1,0,0,0,1},
		{1,0,1,0,0,1,0,0,0,1,1,1,1,1,0,1,1,0,1,1,0,0,1,0,0,0,0,0,0,1,1,1},
		{1,0,1,1,0,0,1,1,1,1,0,0,1,0,0,1,0,0,1,0,0,0,1,0,1,1,1,1,1,1,0,1},
		{1,0,0,0,1,0,0,0,0,0,0,1,1,1,1,1,1,0,1,1,1,1,0,0,1,0,0,0,0,0,0,1},
		{1,0,1,1,1,1,1,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,1,1,1,1},
		{1,0,0,0,1,0,1,0,0,1,1,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,1,0,0,0,0,1},
		{1,0,1,0,1,0,0,0,1,1,0,0,1,0,1,0,0,0,1,0,0,0,1,1,1,1,1,0,1,0,0,1},
		{1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,1},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
	};
	int goal1[2]= { 7, 30};
	int start1[2]= { 21, 1};
	int time1 = 6;

	int *goal = goal1;
	int time = time1;	
	printMaze (screen1, goal, screen, time);

	int x = start1[0];
    int y = start1[1];
	printCharacter(x,y);
 
MARK:
    cicle = cicle + 1;
    if (cicle >= 350) {
    	move = 0;
    	cicle = 1;
		time--;
		printTime(time);
    }

	if (time <= 0)
	{
		zx_setcursorpos(1,12);
    	printf("time out");
		fgetc_cons();        // wait for keypress
 		goto INIT;
	}

    key = getk();
    /*
    if(key!='\0'){
        printf("%02x key",key);
      printf("%d",key);
    }
	*/

	if (move == 1 && cicle % 20 == 0 ) {
		move = 0;
	}

	// TODO remove in release
	if (moveTime == 1 && cicle % 300 == 0 ) {
		moveTime = 0;
	}	

	// TODO remove in release
	if (moveTime == 0 && key == 104) { // h
    	moveTime = 1;
		time = time + 5;
    }

    if (move == 0 && key == 111 && y>1 && GetChar(y-1,x) != 128) { // o
    	move = 1;
		deleteCharacter(x,y);
        y--;
		printCharacter(x,y);
    }

    if (move == 0 && key == 112 && y<30 && GetChar(y+1,x) != 128) { // p
    	move = 1;
        deleteCharacter(x,y);
        y++;
		printCharacter(x,y);
    }

    if (move == 0 && key == 113 && x>1 && GetChar(y,x-1) != 128) { // q
    	move = 1;
        deleteCharacter(x,y);
        x--;        
		printCharacter(x,y);
    }

    if (move == 0 && key == 97 && x<22 && GetChar(y,x+1) != 128) { // a
    	move = 1;
        deleteCharacter(x,y);
        x++;
        printCharacter(x,y);
    }

 	if (checkFinish(x,y,goal) == 1){
 		if (screen == 1) {
 			zx_cls();
 			screen = 2;
			time = time1;	
/*
				zx_setcursorpos(20,10);
	printf("%d",goal1[0]);
	
	zx_setcursorpos(21,10);
	printf("%d",goal1[1]);
*/

			goal = goal1;
			printMaze (screen1, goal, screen, time);
			x = 3;
	    	y = 1;
			printCharacter(x,y);
 		}
		//goal[2]=1;
 		/*
		time = time2;	
		*goal = goal2;
		printMaze (screen2, goal2, 2, time);
		x = 1;
    	y = 1;
		printCharacter(x,y);
		*/
		else 
		{
			//Last		
	    	zx_setcursorpos(1,9);
	    	char* marquesina = "congratulations";

			int lenM = strlen(marquesina) + 1;
			int z;
			for (z = 0; z < lenM; ++z)
			{
				printf("%c",marquesina[z]);
			    in_Pause(20);
			}

	    	//printf("congratulations");
			fgetc_cons();        // wait for keypress

			zx_cls();
	 		goto INIT;
	 		//goto FINISH;
 		}
 	}

    //int w = in_Pause(220);

//    fgetc_cons();        // wait for keypress

    goto MARK;

FINISH:

	zx_cls();

	zx_setcursorpos(5,3);
    printf("nos vemos en mini palote");

	int pausePalote = 5;
	int xPalote = 10;
	for (a = 0; a < 28; a=a+4) //21-3
    {
    	printPalote1(xPalote,a);
    	printMiniPalote1(xPalote+10,a);
    	printMicroPalote1(0,a);
    	printPicoPalote1(4,a);
	    in_Pause(pausePalote);
	    printPalote2(xPalote,a+1);
    	printMiniPalote2(xPalote+10,a+1);
    	printMicroPalote2(0,a+1);
    	printPicoPalote2(4,a+1);
	    in_Pause(pausePalote);
	    printPalote3(xPalote,a+2);
    	printMiniPalote3(xPalote+10,a+2);
    	printMicroPalote1(0,a+2);
    	printPicoPalote3(4,a+2);
	    in_Pause(pausePalote);
	    printPalote4(xPalote,a+3);
    	printMiniPalote4(xPalote+10,a+3);
    	printMicroPalote3(0,a+3);
    	printPicoPalote4(4,a+3);
	    in_Pause(pausePalote);
	}

/*
	deletePalote(xPalote,28);
	deleteMiniPalote(xPalote+10,28);
	deleteMicroPalote(0,28);
	deletePicoPalote(4,28);
*/

/*
    	printPicoPalote1(0,0);
    	printPicoPalote2(0,3);
    	printPicoPalote3(0,6);
    	printPicoPalote4(0,9);
*/

/*
	zx_asciimode(0);
	zx_setcursorpos(2,0);
	printf("%c%c%c", 0, 129, 134);
	zx_setcursorpos(3,0);
	printf("%c%c%c", 0, 132, 7);
	zx_setcursorpos(4,0);
	printf("%c%c%c", 0, 7, 130);

	zx_setcursorpos(2,1);
	printf("%c%c%c", 0, 129, 134);
	zx_setcursorpos(3,1);
	printf("%c%c%c", 0, 132, 7);
	zx_setcursorpos(4,1);
	printf("%c%c%c", 0, 133, 130);

	zx_setcursorpos(2,1);
	printf("%c%c%c", 0, 129, 134);
	zx_setcursorpos(3,8);
	printf("%c%c%c", 0, 132, 7);
	zx_setcursorpos(4,8);
	printf("%c%c%c", 0, 7, 3);
*/
	zx_asciimode(1);
    /*
	zx_setcursorpos(0,0);

    for (a = 0; a < 21; ++a) //21-3
    {
    	printf("01234567890123456789012345678912");
	}

    zx_asciimode(0);
	zx_setcursorpos(2,0);
	printf("%c%c%c%c", 0, 6, 3, 4);
	zx_setcursorpos(3,0);
	printf("%c%c%c%c", 0, 5,133,130);
	zx_setcursorpos(4,0);
	printf("%c%c%c%c", 0, 134,131,1);
	zx_setcursorpos(5,0);
	printf("%c%c%c%c", 0, 133,128,3);
	zx_setcursorpos(6,0);
	printf("%c%c%c%c", 0, 133,128,0);
	zx_setcursorpos(7,0);
	printf("%c%c%c%c", 135, 129,128,0);
	zx_setcursorpos(8,0);
	printf("%c%c%c%c", 2, 0,133,130);

	zx_setcursorpos(2,5);
	printf("%c%c%c%c", 0, 6, 3, 4);
	zx_setcursorpos(3,5);
	printf("%c%c%c%c", 0, 5,133,130);
	zx_setcursorpos(4,5);
	printf("%c%c%c%c", 0, 134,131,1);
	zx_setcursorpos(5,5);
	printf("%c%c%c%c", 0, 133,128,0);
	zx_setcursorpos(6,5);
	printf("%c%c%c%c", 0, 133,128,0);
	zx_setcursorpos(7,5);
	printf("%c%c%c%c", 0, 133,128,0);
	zx_setcursorpos(8,5);
	printf("%c%c%c%c", 0, 133,128,130);

	zx_setcursorpos(2,10);
	printf("%c%c%c%c", 0, 6, 3, 4);
	zx_setcursorpos(3,10);
	printf("%c%c%c%c", 0, 5,133,130);
	zx_setcursorpos(4,10);
	printf("%c%c%c%c", 0, 134,131,1);
	zx_setcursorpos(5,10);
	printf("%c%c%c%c", 133, 132,128,0);
	zx_setcursorpos(6,10);
	printf("%c%c%c%c", 0, 133,128,0);
	zx_setcursorpos(7,10);
	printf("%c%c%c%c", 135, 129,128,0);
	zx_setcursorpos(8,10);
	printf("%c%c%c%c", 2, 0,133,130);

	zx_setcursorpos(2,15);
	printf("%c%c%c%c", 0, 6, 3, 4);
	zx_setcursorpos(3,15);
	printf("%c%c%c%c", 0, 5,133,130);
	zx_setcursorpos(4,15);
	printf("%c%c%c%c", 0, 134,131,1);
	zx_setcursorpos(5,15);
	printf("%c%c%c%c", 2, 132,128,132);
	zx_setcursorpos(6,15);
	printf("%c%c%c%c", 0, 133,128,0);
	zx_setcursorpos(7,15);
	printf("%c%c%c%c", 135, 129,128,131);
	zx_setcursorpos(8,15);
	printf("%c%c%c%c", 2, 0,0,0);
*/

/*
	zx_setcursorpos(2,5);
    zx_asciimode(0);
	printf("%c%c%c%c",135,3,3,4);
	zx_setcursorpos(3,5);
	printf("%c%c%c%c", 133,0,133,130);
	zx_setcursorpos(4,5);
	printf("%c%c%c%c", 2,131,131,1);
	zx_setcursorpos(5,5);
	printf("%c%c%c%c", 131,128,128,131);
	zx_setcursorpos(6,5);
	printf("%c%c%c%c", 5,128,128,0);
	zx_setcursorpos(7,5);
	printf("%c%c%c%c", 0,128,128,0);	
	zx_setcursorpos(8,5);
	printf("%c%c%c%c", 7,3,128,0);
	zx_setcursorpos(9,5);
	printf("%c%c%c%c", 0,0,133,131);

	zx_setcursorpos(2,10);
    zx_asciimode(0);
	printf("%c%c%c%c",135,3,3,4);
	zx_setcursorpos(3,10);
	printf("%c%c%c%c", 133,0,133,130);
	zx_setcursorpos(4,10);
	printf("%c%c%c%c", 2,131,131,1);
	zx_setcursorpos(5,10);
	printf("%c%c%c%c", 131,128,128,131);
	zx_setcursorpos(6,10);
	printf("%c%c%c%c", 5,128,128,0);
	zx_setcursorpos(7,10);
	printf("%c%c%c%c", 0,128,128,0);	
	zx_setcursorpos(8,10);
	printf("%c%c%c%c", 7,3,128,0);
	zx_setcursorpos(9,10);
	printf("%c%c%c%c", 0,0,133,131);

    zx_asciimode(1);

*/

	fgetc_cons();        // wait for keypress
 	
 	goto INIT;

	return 0;
}

