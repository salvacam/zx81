#include <stdio.h>
#include <stdlib.h>
#include <zx81.h>
void in_Pause(uint msec);
char strlen(const char *s);
uint in_KeyPressed(uint scancode);


char *display;
extern int d_file @16396;

uchar GetChar(char cY, char cX)
{
	return wpeek(wpeek(16396) + 1 + cX + cY * 33);
}

void SetChar(char cX, char cY, char cValue) {
	bpoke(wpeek(16396) + 1 + cY + cX * 33, cValue);
}

void printStar(char x, char y) {
	do {
		zx_setcursorpos(x, y);
		zx_asciimode(1);
		printf("PRESS ANY KEY TO START");
		zx_asciimode(0);
		zx_setcursorpos(x, y + 5);
		printf("%c", 128);
		zx_setcursorpos(x, y + 9);
		printf("%c", 128);
		zx_setcursorpos(x, y + 13);
		printf("%c", 128);
		zx_setcursorpos(x, y + 16);
		printf("%c", 128);
		zx_asciimode(1);
		in_Pause(75);
		zx_setcursorpos(x, y);
		printf("press any key to start");
		in_Pause(75);
	} while (getk() == '\0');
}

void printCenter(char numLiner, char* textCenter) {
	char lenCenter = strlen(textCenter);
	char whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner, whiteSpace);
	printf("%s", textCenter);
}

void printLineClean(char line) {
	bpoke(wpeek(16396) + 1 + 3 + (line * 33), 136);
	bpoke(wpeek(16396) + 1 + 4 + (line * 33), 136);

	bpoke(wpeek(16396) + 1 + 27 + (line * 33), 136);
	bpoke(wpeek(16396) + 1 + 28 + (line * 33), 136);
}

void printLine(char line, char linePrint[4]) {
	bpoke(wpeek(16396) + 1 + 3 + (line * 33), 136);
	bpoke(wpeek(16396) + 1 + 4 + (line * 33), 136);

	char y;
	for (y = 0; y < 4; y++) {
		bpoke(wpeek(16396) + 1 + linePrint[y] + (line * 33), 136);
	}

	bpoke(wpeek(16396) + 1 + 27 + (line * 33), 136);
	bpoke(wpeek(16396) + 1 + 28 + (line * 33), 136);
}

char valueinarray(char val, char array[6]) {
	char y;
	for (y = 0; y < 6; y++) {
		if (array[y] == val) return 1;
	}
	return 0;
}

void printCharacter (char x, char y)
{
	SetChar(x, y,   133);
	SetChar(x, y+1, 134);

	SetChar(x+1, y,   133);
	SetChar(x+1, y+1, 5);

	SetChar(x+2, y,   133);
	SetChar(x+2, y+1, 130);
}

void deleteCharacter (char x, char y)
{
	SetChar(x, y,   0);
	SetChar(x, y+1, 0);

	SetChar(x+1, y,   0);
	SetChar(x+1, y+1, 0);

	SetChar(x+2, y,   0);
	SetChar(x+2, y+1, 0);
}

void deleteCharacterLeft (char x, char y)
{
	SetChar(x, y,   0);

	SetChar(x+1, y,   0);

	SetChar(x+2, y,   0);
}

void deleteCharacterRight (char x, char y)
{
	SetChar(x, y+1, 0);

	SetChar(x+1, y+1, 0);

	SetChar(x+2, y+1, 0);
}

unsigned char xpos [];
unsigned char ypos [];
unsigned char a [];
unsigned char d [];
unsigned char e [];
unsigned char b [];

#asm
	._xpos
		defb 0
	._ypos
		defb 0
	._a
		defb 0
	._d
		defb 0
	._e
		defb 0
	._b
		defb 0

#endasm

void printCharacterAsm(unsigned char x, unsigned char y, unsigned char pos)
{
	xpos [0] = x;
	ypos [0] = y;

	if (pos == 1) {
		a [0] = 129;
		d [0] = 5; //130; //5;
		e [0] = 129;
		b [0] = 134;
	} else {	
		a [0] = 133;//129; // 133;	
		d [0] = 130;
		e [0] = 6;
		b [0] = 130;
	}
  
 	#asm 
		ld	hl, (16396)	; D_FILE

		ld a, (_xpos)
		and	a						; ¿a == 0?
		jr	z, z81tpzero

		ld b, a
		ld de, 33
	
	.z81tpl1
		add	hl, de
		djnz z81tpl1				; hl = buffer + 32 * x
	
	.z81tpzero
		ld a, (_ypos)
		add hl, a;	
		ld a,155 					;133 
		ld (hl),a

		add hl, 1
		ld b, (155) 					;134 
		ld (hl),b

		add hl, 32
		ld a, (_a) 					;133 
		ld (hl),a

		add hl, 1
		ld a, (_d)					;5
		ld (hl),a

		add hl, 32
		ld a, (_e)				;133
		ld (hl),a

		add hl, 1	
		ld a, (_b)				;130
		ld (hl),a

	#endasm;
}

void deleteCharacterAsm(unsigned char x, unsigned char y) {
	xpos [0] = x;
	ypos [0] = y;
	#asm
		ld	hl, (16396)	; D_FILE
		
		ld a, (_xpos)
		and	a						; ¿a == 0?
		jr	z, z81tpzero1

		ld b, a
		ld de, 33
	
	.z81tpl2
		add	hl, de
		djnz z81tpl2				; hl = buffer + 32 * x
		
	.z81tpzero1
		ld a, (_ypos)
		add hl, a;	
		ld b,0 
		ld (hl),b

		add hl, 1
		ld (hl),b

		add hl, 32
		ld (hl),b

		add hl, 1
		ld (hl),b

		add hl, 32
		ld (hl),b

		add hl, 1
		ld (hl),b

	#endasm

}

int main(void)
{
/*
	zx_asciimode(0);
 	zx_setcursorpos(10, 10);
	printf("%c%c", 155, 155);
 	zx_setcursorpos(11, 10);
	printf("%c%c", 129, 130);
 	zx_setcursorpos(12, 10);
	printf("%c%c", 129, 134);

 	zx_setcursorpos(10, 15);
	printf("%c%c", 155, 155);
 	zx_setcursorpos(11, 15);
	printf("%c%c", 129, 130);
 	zx_setcursorpos(12, 15);
	printf("%c%c", 6, 130);


 	zx_setcursorpos(15, 15);
	printf("%c%c", 7, 132);
 	zx_setcursorpos(16, 15);
	printf("%c%c", 132, 7);
 	zx_setcursorpos(17, 15);
	printf("%c%c", 129, 7);
	*/

	//int e = 609;
/*
	unsigned char e = 1;
	char i;
	for (i = 0; i < 200; i++) {
	

		deleteCharacterAsm(e);
		printCharacterAsm(e);
	}

	fgetc_cons();        // wait for keypress
*/

	unsigned char x = 1, y = 1, pos = 1;

/*
	zx_asciimode(1);
 	zx_setcursorpos(18, 18);
	printf("%d",a);
*/


	//int pos = 1;
	//printCharacter(x, y);
	printCharacterAsm(x,y,pos);
/*

	for (i = 0; i < 140; i++) {
	    deleteCharacter(x,y);	
		printCharacter(x,y);
	}

	fgetc_cons();        // wait for keypress
*/
INITGAME:

	if (in_KeyPressed(in_LookupKey('O')) && y > 1) {
		pos++;
		if (pos>2)pos=1;

		deleteCharacterAsm(x,y);
        y--;
		printCharacterAsm(x,y,pos);
    }

    if (in_KeyPressed(in_LookupKey('P')) && y < 30) {
		pos++;
		if (pos>2)pos=1;
		
		//asm
		deleteCharacterAsm(x,y);
        y++;
		printCharacterAsm(x,y,pos);
    }

    if (in_KeyPressed(in_LookupKey('Q')) && x > 1) {
		pos++;
		if (pos>2)pos=1;

        deleteCharacterAsm(x,y);
        x--;
        printCharacterAsm(x,y,pos);
    }

    if (in_KeyPressed(in_LookupKey('A')) && x < 19) {
		pos++;
		if (pos>2)pos=1;

		deleteCharacterAsm(x,y);
        x++;
		printCharacterAsm(x,y,pos);
    }


    //in_Pause(200);
	in_Wait(10);


	//fgetc_cons();        // wait for keypress


goto INITGAME;


	return 0;
}